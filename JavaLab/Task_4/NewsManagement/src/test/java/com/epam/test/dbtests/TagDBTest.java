package com.epam.test.dbtests;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.service.MainService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.dbunit.DatabaseUnitException;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/resources/SpringXMLConfig.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
public class TagDBTest {
    
    @Autowired
    private MainService service;
    
    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/Tag/TagData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/Tag/TagData_Create_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testCreate() throws DAOException, DatabaseUnitException, ServiceException {
        this.service.addTag(1, "TAG2");
    }

    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/Tag/TagData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/Tag/TagData_Delete_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testDelete() throws DAOException, DatabaseUnitException, ServiceException {
        this.service.deleteTag(1);
    }
    
    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/Tag/TagData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/Tag/TagData_Read_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testRead() throws DAOException, DatabaseUnitException, ServiceException {
        assertEquals("TAG1", this.service.getTagByID(1).getTagName());
    }
    
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/Tag/TagData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/Tag/TagData_Read_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void readAllTagsOnNews() throws DAOException, DatabaseUnitException, ServiceException {
        assertEquals(1, this.service.getAllTagsOnNews(1).size());
            assertEquals("TAG1", this.service.getAllTagsOnNews(1).get(0).getTagName());
    }

    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/Tag/TagData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/Tag/TagData_Update_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testUpdate() throws DAOException, DatabaseUnitException, ServiceException {
        this.service.editTag(1, "NEW_TAG1");
    }
}
