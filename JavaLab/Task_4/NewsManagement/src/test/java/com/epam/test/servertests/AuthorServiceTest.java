package com.epam.test.servertests;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dao.impl.AuthorDAOImpl;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
    
    @Mock
    private AuthorDAOImpl authorDAO;
    @InjectMocks
    private AuthorServiceImpl authorService;
    private final static ApplicationContext applicationContext = new ClassPathXmlApplicationContext("SpringXMLConfig.xml");

    @Before
    public void init(){
        authorService = (AuthorServiceImpl) applicationContext.getBean("authorService");
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testAddAuthor() throws DAOException, ServiceException{
        int a = authorService.addAuthorByName("Karen");
        InOrder inOrder = inOrder(authorDAO);
        inOrder.verify(authorDAO).create((Author) anyObject());
        assertEquals(false, a != 0);
    }
     
    @Test
    public void testDeleteAuthor() throws ServiceException, DAOException{
        boolean b = authorService.deleteAuthor(1);
        InOrder inOrder = inOrder(authorDAO);
        inOrder.verify(authorDAO).delete(1);
        assertEquals(false, b);
    }
    
    @Test
    public void testEditAuthor() throws ServiceException, DAOException{
        boolean b = authorService.editAuthor(1, "check");
        InOrder inOrder = inOrder(authorDAO);
        inOrder.verify(authorDAO).update((Author) anyObject());
        assertEquals(false, b);
    }
    
    @Test
    public void testGetAuthor() throws ServiceException, DAOException{
        Author author = authorService.getAuthor(1);
        InOrder inOrder = inOrder(authorDAO);
        inOrder.verify(authorDAO).read(1);
        assertEquals(null, author);
    }
 
 
 

}
