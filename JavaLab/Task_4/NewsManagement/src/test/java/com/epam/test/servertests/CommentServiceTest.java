package com.epam.test.servertests;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dao.impl.CommentDAOImpl;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
    
    @Mock
    private CommentDAOImpl commentDAO;
    @InjectMocks
    private CommentServiceImpl commentService;
    private final static ApplicationContext applicationContext = new ClassPathXmlApplicationContext("SpringXMLConfig.xml");

    @Before
    public void init(){
        commentService = (CommentServiceImpl) applicationContext.getBean("commentService");
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void addComments() throws ServiceException, DAOException {
        int q = commentService.addComments(1, "comment");
        InOrder inOrder = inOrder(commentDAO);
        inOrder.verify(commentDAO).create((Comment) anyObject());
    }

//    @Test
//    public void deleteComments() throws ServiceException {
//        commentService.deleteComments(1);
//    }
//
//    @Test
//    public void deleteCommentsInNews() throws ServiceException {
//        commentService.deleteCommentsInNews(1);
//    }
//
//    @Test
//    public void editComments() throws ServiceException {
//        commentService.editComments(1, "comment1");
//    }
//
//    @Test
//    public void getComments() throws ServiceException {
//        commentService.getComments(1);
//    }
}
