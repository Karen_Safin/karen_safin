package com.epam.test.exception;

public class DBUnitTestException extends RuntimeException{

    public DBUnitTestException() {
    }

    public DBUnitTestException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
    
}
