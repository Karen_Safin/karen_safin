package com.epam.test.dbtests;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.service.MainService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.dbunit.DatabaseUnitException;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/resources/SpringXMLConfig.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
public class CommentDBTest {
    
    @Autowired
    private MainService service;
    
    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/Comment/CommentData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/Comment/CommentData_Create_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testCreate() throws DAOException, DatabaseUnitException, ServiceException {
        this.service.addComments(3, "COMMENT3");
    }

    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/Comment/CommentData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/Comment/CommentData_Delete_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testDelete() throws DAOException, DatabaseUnitException, ServiceException {
        this.service.deleteComments(1);
    }
    
    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/Comment/CommentData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/Comment/CommentData_Read_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testRead() throws DAOException, DatabaseUnitException, ServiceException {
        assertEquals(2, this.service.getComments(2).size());
        for(int q = 0; q < this.service.getComments(2).size(); q++){
            assertEquals("COMMENT", this.service.getComments(2).get(q).getCommentText());
        }
    }

    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/Comment/CommentData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/Comment/CommentData_Update_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testUpdate() throws DAOException, DatabaseUnitException, ServiceException {
        this.service.editComments(1, "NEW_COMMENT");
    }
    
    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/Comment/CommentData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/Comment/CommentData_DeleteInNews_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void deleteCommentsInNews() throws DAOException, DatabaseUnitException, ServiceException {
        this.service.deleteCommentsInNews(2);
    }
    
}
