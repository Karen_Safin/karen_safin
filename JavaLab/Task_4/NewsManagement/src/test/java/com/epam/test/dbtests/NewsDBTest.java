package com.epam.test.dbtests;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.service.MainService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import java.util.Date;
import org.dbunit.DatabaseUnitException;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/resources/SpringXMLConfig.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
public class NewsDBTest {
    
    @Autowired
    private MainService service;
    
    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/News/NewsData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/News/NewsData_Create_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testCreate() throws DAOException, DatabaseUnitException, ServiceException {
        Date date = new Date(System.currentTimeMillis());
        this.service.addNews("CCC", "CCC", "CCC", date);
    }

    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/News/NewsData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/News/NewsData_Delete_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testDelete() throws DAOException, DatabaseUnitException, ServiceException {
        this.service.deleteNews(2);
    }
    
    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/News/NewsData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/News/NewsData_Read_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testRead() throws DAOException, DatabaseUnitException, ServiceException {
            assertEquals("AAA", this.service.getNews(1).getTitle());
    }

    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/News/NewsData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/News/NewsData_Update_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testUpdate() throws DAOException, DatabaseUnitException, ServiceException {
        Date date = new Date(System.currentTimeMillis());
        this.service.editNews(2, "CCC", "DDD", "BBB", date);
    }
    
    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/News/NewsData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/News/NewsData_SelectAllNewses_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void selectAllNewses() throws DAOException, DatabaseUnitException, ServiceException {
        assertEquals(2, this.service.getAllNewses().size());
        assertEquals("AAA", this.service.getAllNewses().get(0).getTitle());
        assertEquals("BBB", this.service.getAllNewses().get(1).getTitle());
    }

    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/News/NewsData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/News/NewsData_SelectByAuthor_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void selectByAuthor() throws DAOException, DatabaseUnitException, ServiceException {
        assertEquals(1, this.service.getNewsesByAuthor(1).size());
        assertEquals("AAA", this.service.getNewsesByAuthor(1).get(0).getTitle());
        assertEquals("BBB", this.service.getNewsesByAuthor(2).get(0).getTitle());
    }
    
    /**
     *
     * @throws DAOException
     * @throws DatabaseUnitException
     * @throws ServiceException
     */
    @Test
    @DatabaseSetup(value = "file:src/test/resources/xmldata/News/NewsData.xml")
    @ExpectedDatabase(value = "file:src/test/resources/xmldata/News/NewsData_SelectByTag_Expected.xml",
                    assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void selectByTag() throws DAOException, DatabaseUnitException, ServiceException {
        assertEquals(1, this.service.getNewsesByTag(1).size());
        assertEquals("AAA", this.service.getNewsesByTag(1).get(0).getTitle());
        assertEquals("BBB", this.service.getNewsesByTag(2).get(0).getTitle());
    }

}
