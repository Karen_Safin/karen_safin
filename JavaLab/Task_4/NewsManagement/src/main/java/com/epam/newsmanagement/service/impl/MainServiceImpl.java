package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.service.MainService;
import com.epam.newsmanagement.service.exception.ServiceException;
import java.util.Date;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

public class MainServiceImpl implements MainService{
    private AuthorServiceImpl authorService;
    private CommentServiceImpl commentService;
    private NewsServiceImpl newsService;
    private TagServiceImpl tagService;

    public MainServiceImpl(AuthorServiceImpl authorService, CommentServiceImpl commentService, NewsServiceImpl newsService, TagServiceImpl tagService) {
        this.authorService = authorService;
        this.commentService = commentService;
        this.newsService = newsService;
        this.tagService = tagService;
    }

    /**
     *
     * @param idNews
     * @param nameOfAuthor
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean addAuthor(int idNews, String nameOfAuthor) throws ServiceException {
        int id = authorService.addAuthorByName(nameOfAuthor);
        boolean relationAddedIsSuccess = newsService.addRelationForAuthorAndNews(idNews, id);
        return id != 0 && relationAddedIsSuccess;
    }
    
    /**
     *
     * @param idAuthor
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean deleteAuthor(int idAuthor) throws ServiceException {
        boolean authorDeletedIsSuccess = authorService.deleteAuthor(idAuthor);
        boolean relationDeletedIsSuccess = newsService.deleteRelationForAuthorAndNewsByAuthor(idAuthor);
        return authorDeletedIsSuccess && relationDeletedIsSuccess;
    }
    
    /**
     *
     * @param idAuthor
     * @param nameOfAuthor
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean editAuthor(int idAuthor, String nameOfAuthor) throws ServiceException {
        return authorService.editAuthor(idAuthor, nameOfAuthor);
    }
    
    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public Author getAuthor(int idNews) throws ServiceException {
        return authorService.getAuthor(idNews);
    }
    
    /**
     *
     * @param idNews
     * @param commentText
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public int addComments(int idNews, String commentText) throws ServiceException {
        return commentService.addComments(idNews, commentText);
    }
    
    /**
     *
     * @param idComment
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean deleteComments(int idComment) throws ServiceException {
        return commentService.deleteComments(idComment);
    }
    
    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean deleteCommentsInNews(int idNews) throws ServiceException {
        return commentService.deleteCommentsInNews(idNews);
    }
    
    /**
     *
     * @param idComment
     * @param commentText
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean editComments(int idComment, String commentText) throws ServiceException {
        return commentService.editComments(idComment, commentText);
    }
    
    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public List<Comment> getComments(int idNews) throws ServiceException {
        return commentService.getComments(idNews);
    }
    
    /**
     *
     * @param shortText
     * @param fullText
     * @param title
     * @param creationDate
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public int addNews(String shortText, String fullText, String title, Date creationDate) throws ServiceException {
        return newsService.addNews(shortText, fullText, title, creationDate);
    }
    
    /**
     *
     * @param idNews
     * @param shortText
     * @param fullText
     * @param title
     * @param modificationDate
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean editNews(int idNews, String shortText, String fullText, String title, Date modificationDate) throws ServiceException {
        return newsService.editNews(idNews, shortText, fullText, title, modificationDate);
    }
    
    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean deleteNews(int idNews) throws ServiceException {
        boolean relationDeletedIsSuccessA = newsService.deleteRelationForAuthorAndNewsByNews(idNews);
        boolean relationDeletedIsSuccessT = newsService.deleteRelationForTagAndNewsByNews(idNews);
        boolean deletedNewsIsSuccess = newsService.deleteNews(idNews);
        return deletedNewsIsSuccess && relationDeletedIsSuccessA && relationDeletedIsSuccessT;
    }
    
    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public News getNews(int idNews) throws ServiceException {
        return newsService.getNews(idNews);
    }
    
    /**
     *
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public List<News> getAllNewses() throws ServiceException {
        return newsService.getAllNewses();
    }
    
    /**
     *
     * @param idAuthor
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public List<News> getNewsesByAuthor(int idAuthor) throws ServiceException {
        return newsService.getNewsesByAuthor(idAuthor);
    }
    
    /**
     *
     * @param idTag
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public List<News> getNewsesByTag(int idTag) throws ServiceException {
        return newsService.getNewsesByTag(idTag);
    }
    
    /**
     *
     * @param idNews
     * @param tagName
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean addTag(int idNews, String tagName) throws ServiceException {
        int id = tagService.addTag(tagName);
        boolean relationAddedIsSuccess = newsService.addRelationForTagAndNews(idNews, id);
        return id != 0 && relationAddedIsSuccess;
    }
    
    /**
     *
     * @param idTag
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean deleteTag(int idTag) throws ServiceException {
        boolean tagDeletedIsSuccess = authorService.deleteAuthor(idTag);
        boolean relationDeletedIsSuccess = newsService.deleteRelationForTagAndNewsByTag(idTag);
        return tagDeletedIsSuccess && relationDeletedIsSuccess;
    }
    
    /**
     *
     * @param idTag
     * @param tagName
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public boolean editTag(int idTag, String tagName) throws ServiceException {
        return tagService.editTag(idTag, tagName);
    }
    
    /**
     *
     * @param idTag
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public Tag getTagByID(int idTag) throws ServiceException {
        return tagService.getTagByID(idTag);
    }
    
    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public List<Tag> getAllTagsOnNews(int idNews) throws ServiceException {
        return tagService.getAllTagsOnNews(idNews);
    }
    
}
