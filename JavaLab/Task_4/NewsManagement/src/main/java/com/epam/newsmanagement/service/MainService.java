package com.epam.newsmanagement.service;

import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;
import java.util.Date;
import java.util.List;

public interface MainService {
    
    public boolean addAuthor(int idNews, String nameOfAuthor) throws ServiceException;
    public boolean addTag(int idNews, String tag) throws ServiceException;
    public int addComments(int idNews, String commentText) throws ServiceException;
    public int addNews(String shortText, String fullText, String title, Date creationDate) throws ServiceException;
    
    public boolean deleteAuthor(int idAuthor) throws ServiceException;
    public boolean deleteComments(int idComment) throws ServiceException;
    public boolean deleteCommentsInNews(int idNews) throws ServiceException;
    public boolean deleteNews(int idNews) throws ServiceException;
    public boolean deleteTag(int idTag) throws ServiceException;
    
    public boolean editAuthor(int idAuthor, String nameOfAuthor) throws ServiceException;
    public boolean editComments(int idComment, String commentText) throws ServiceException;
    public boolean editNews(int idNews, String shortText, String fullText,  String title, Date modificationDate) throws ServiceException;
    public boolean editTag(int idTag, String tag) throws ServiceException;
    
    public Author getAuthor(int idNews) throws ServiceException;
    public News getNews(int idNews) throws ServiceException;
    public Tag getTagByID(int idTag) throws ServiceException;
    public List<Comment> getComments(int idNews) throws ServiceException;
    public List<Tag> getAllTagsOnNews(int idNews) throws ServiceException;
    public List<News> getAllNewses() throws ServiceException;
    public List<News> getNewsesByAuthor(int idAuthor) throws ServiceException;
    public List<News> getNewsesByTag(int idTag) throws ServiceException;
    
}
