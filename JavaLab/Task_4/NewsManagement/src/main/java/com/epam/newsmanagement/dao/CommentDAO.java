package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.model.Comment;
import java.util.List;

public interface CommentDAO extends CrudDAO<Comment, Integer>{

    @Override
    public boolean delete(Integer id) throws DAOException;
    @Override
    public boolean update(Comment t) throws DAOException;
    @Override
    public Comment read(Integer id) throws DAOException;
    @Override
    public Integer create(Comment t) throws DAOException;
    
    public boolean deleteCommentsInNews(Integer id) throws DAOException;
    public List<Comment> readOnNews(Integer idNews) throws DAOException;
    
    
}
