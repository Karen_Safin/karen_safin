package com.epam.newsmanagement.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class News implements Serializable {
    private static final long serialVersionUID = 1L;
    private int newsId;
    private String shortText;
    private String fullText;
    private String title;
    private Date creationDate;
    private Date modificationDate;

    public News() {
    }

    public News(int newsId) {
        this.newsId = newsId;
    }

    public News(String shortText, String fullText, String title, Date creationDate) {
        this.newsId = 0;
        this.shortText = shortText;
        this.fullText = fullText;
        this.title = title;
        this.creationDate = creationDate;
        this.modificationDate = null;
    }

    public News(int newsId, String shortText, String fullText, String title, Date modificationDate) {
        this.newsId = newsId;
        this.shortText = shortText;
        this.fullText = fullText;
        this.title = title;
        this.modificationDate = modificationDate;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final News other = (News) obj;
        if (this.newsId != other.newsId) {
            return false;
        }
        if (!Objects.equals(this.shortText, other.shortText)) {
            return false;
        }
        if (!Objects.equals(this.fullText, other.fullText)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.creationDate, other.creationDate)) {
            return false;
        }
        if (!Objects.equals(this.modificationDate, other.modificationDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "News{" + "newsId=" + newsId + ", shortText=" + shortText + ", fullText=" + fullText + ", title=" + title + ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + '}';
    }

}
