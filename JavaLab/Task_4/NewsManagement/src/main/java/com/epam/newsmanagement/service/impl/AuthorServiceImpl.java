package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dao.impl.AuthorDAOImpl;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.exception.ServiceException;


public class AuthorServiceImpl implements AuthorService{
    
    private AuthorDAOImpl authorDAO;

    public AuthorServiceImpl(AuthorDAOImpl authorDAO) {
        this.authorDAO = authorDAO;
    }

    /**
     *
     * @param nameOfAuthor
     * @return
     * @throws ServiceException
     */
    @Override
    public int addAuthorByName(String nameOfAuthor) throws ServiceException {
        Author author = new Author(nameOfAuthor);
        try {
           return authorDAO.create(author);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idAuthor
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean deleteAuthor(int idAuthor) throws ServiceException {
        try {
            return authorDAO.delete(idAuthor);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idAuthor
     * @param nameOfAuthor
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean editAuthor(int idAuthor, String nameOfAuthor) throws ServiceException {
        Author author = new Author(idAuthor, nameOfAuthor);
        try {
            return authorDAO.update(author);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Override
    public Author getAuthor(int idNews) throws ServiceException {
        Author author = null;
        try {
            author = authorDAO.read(idNews);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
        return author;
    }

}
