package com.epam.newsmanagement.service.exception;

public class ServiceException extends Exception{

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage(); 
    }
    
}
