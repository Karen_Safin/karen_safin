package com.epam.newsmanagement.service;

import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.service.exception.ServiceException;
import java.util.Date;
import java.util.List;

public interface NewsService {
    
    public int addNews(String shortText, String fullText, String title, 
                            Date creationDate) throws ServiceException;
    public boolean editNews(int idNews, String shortText, 
                            String fullText,  String title, 
                            Date modificationDate) throws ServiceException;
    public boolean deleteNews(int idNews) throws ServiceException;
    public News getNews(int idNews) throws ServiceException;
    public List<News> getAllNewses() throws ServiceException;
    public List<News> getNewsesByAuthor(int idAuthor) throws ServiceException;
    public List<News> getNewsesByTag(int idTag) throws ServiceException;
    public boolean addRelationForAuthorAndNews(Integer idNews, Integer idAuthor) throws ServiceException;
    public boolean addRelationForTagAndNews(Integer idNews, Integer idTag) throws ServiceException;
    public boolean deleteRelationForAuthorAndNewsByAuthor(Integer idAuthor) throws ServiceException;
    public boolean deleteRelationForTagAndNewsByTag(Integer idTag) throws ServiceException;
    public boolean deleteRelationForAuthorAndNewsByNews(Integer idAuthor) throws ServiceException;
    public boolean deleteRelationForTagAndNewsByNews(Integer idTag) throws ServiceException;
}
