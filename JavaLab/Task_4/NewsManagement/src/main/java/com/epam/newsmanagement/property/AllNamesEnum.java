package com.epam.newsmanagement.property;

public enum AllNamesEnum {

    NEWS_TABLE_NAME("JIM.NEWS"),
    AUTHOR_TABLE_NAME("JIM.AUTHOR"),
    COMMENTS_TABLE_NAME("JIM.COMMENTS"),
    TAG_TABLE_NAME("JIM.TAG"),
    NEWS_TAG_TABLE_NAME("JIM.NEWS_TAG"),
    NEWS_AUTHOR_TABLE_NAME("JIM.NEWS_AUTHOR"),
    NEWS_AUTHOR_ID("NEWS_AUTHOR_ID"),
    NEWS_TAG_ID("NEWS_TAG_ID"),
    NEWS_ID("NEWS_ID"),
    AUTHOR_ID("AUTHOR_ID"),
    COMMENTS_COMMENTS_ID("COMMENTS.COMMENTS_ID"),
    AUTHOR_AUTHOR_ID("AUTHOR.AUTHOR_ID"),
    NEWS_AUTHOR_NEWS_ID("NEWS_AUTHOR.NEWS_ID"),
    TAG_ID("TAG_ID"),
    SHORT_TEXT("SHORT_TEXT"),
    FULL_TEXT("FULL_TEXT"),
    TITLE("TITLE"),
    CREATION_NEWS_DATE("CREATION_DATE"),
    MODIFICATION_DATE("MODIFICATION_DATE"),
    AUTHOR_NAME("NAME"),
    TAG_NAME("TAG_NAME"),
    COMMENTS_TEXT("COMMENTS_TEXT"),
    COMMENTS_ID("COMMENTS_ID"),
    CREATION_COMMENTS_DATE("CREATION_DATE");
    
    
    private final String value;

    private AllNamesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
