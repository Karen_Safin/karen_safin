package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.property.AllNamesEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;

public class AuthorDAOImpl implements AuthorDAO{
    
    private DataSource dataSource;

    public AuthorDAOImpl() {
    }
    
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     *
     * @param author
     * @return id added author in case of error null.
     * @throws DAOException
     */
    @Override
    public Integer create(Author author) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Integer rezId = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO " 
                    + AllNamesEnum.AUTHOR_TABLE_NAME.getValue()+" ("
                    + AllNamesEnum.AUTHOR_ID.getValue() +", \""
                    + AllNamesEnum.AUTHOR_NAME.getValue() +"\")" +
                    " VALUES (AUTHOR_SEQ.nextval, ?)", 
                    new String[] {AllNamesEnum.AUTHOR_ID.getValue()});
            preparedStatement.setString(1, author.getName());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            rezId = resultSet.getInt(1);
        } catch (SQLException e) {
            throw new DAOException("DAOException in create() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in create() method");
            }
        }
        return rezId;
    }

    /**
     *
     * @param idNews
     * @return Author in case of error null
     * @throws DAOException
     */
    @Override
    public Author read(Integer idNews) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Author rezAuthor = new Author();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT "
                    + AllNamesEnum.AUTHOR_ID.getValue()+", \""
                    + AllNamesEnum.AUTHOR_NAME.getValue()+"\" FROM " 
                    + AllNamesEnum.AUTHOR_TABLE_NAME.getValue() + " WHERE " 
                    + AllNamesEnum.AUTHOR_AUTHOR_ID.getValue() + " = (SELECT "
                    + AllNamesEnum.AUTHOR_ID.getValue()+" FROM " 
                    + AllNamesEnum.NEWS_AUTHOR_TABLE_NAME.getValue() + " WHERE " 
                    + AllNamesEnum.NEWS_AUTHOR_NEWS_ID.getValue() + " = ?)");
            preparedStatement.setInt(1, idNews);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Author author = new Author();
                author.setAuthorId(resultSet.getInt(AllNamesEnum.AUTHOR_ID.getValue()));
                author.setName(resultSet.getString(AllNamesEnum.AUTHOR_NAME.getValue()));
                rezAuthor = author;
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in read() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in read() method");
            }
        }
        return rezAuthor;
    }

    /**
     *
     * @param author
     * @return
     * @throws DAOException
     */
    @Override
    public boolean update(Author author) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countMod = 0;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE " 
                    + AllNamesEnum.AUTHOR_TABLE_NAME.getValue() + " SET \""
                    + AllNamesEnum.AUTHOR_NAME.getValue() + "\" = ? WHERE "
                    + AllNamesEnum.AUTHOR_ID.getValue() + "= ?");
            preparedStatement.setString(1, author.getName());
            preparedStatement.setInt(2, author.getAuthorId());
            countMod = preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new DAOException("DAOException in update() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in update() method");
            }
        }
        return countMod == 1;
    }

    /**
     *
     * @param idAuthor
     * @return
     * @throws DAOException
     */
    @Override
    public boolean delete(Integer idAuthor) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countDel = 0;
        try {
            if (idAuthor != null) {
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("DELETE FROM " 
                        + AllNamesEnum.AUTHOR_TABLE_NAME.getValue() + " WHERE "
                        + AllNamesEnum.AUTHOR_ID.getValue() + " = ?");
                preparedStatement.setInt(1, idAuthor);
                countDel = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in delete() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in delete() method");
            }
        } 
        return countDel == 1;
    }
    
    /**
     *
     * @param connection
     * @param statement
     * @param resultSet
     * @return
     * @throws DAOException
     */
    public boolean closeAll(Connection connection, 
                            Statement  statement, 
                            ResultSet  resultSet) throws DAOException{
        boolean isCloseOK = true;
        if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close resultSet (" + e.getMessage() + ") ");
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close statement (" + e.getMessage() + ") ");
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close connection (" + e.getMessage() + ") ");
                }
            }
        return isCloseOK;
    }
}