
package com.epam.newsmanagement.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private int commentId;
    private String commentText;
    private Date creationDate;
    private int newsId;

    public Comment() {
    }

    public Comment(int commentId) {
        this.commentId = commentId;
    }

    public Comment(String commentText, int newsId) {
        this.commentId = 0;
        this.commentText = commentText;
        this.newsId = newsId;
    }

    public Comment(int commentId, String commentText) {
        this.commentId = commentId;
        this.commentText = commentText;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Comment other = (Comment) obj;
        if (this.commentId != other.commentId) {
            return false;
        }
        if (!Objects.equals(this.commentText, other.commentText)) {
            return false;
        }
        if (!Objects.equals(this.creationDate, other.creationDate)) {
            return false;
        }
        if (!Objects.equals(this.newsId, other.newsId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Comment{" + "commentId=" + commentId + ", commentText=" + commentText + ", creationDate=" + creationDate + ", newsId=" + newsId + '}';
    }
    
    
    
}
