package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.model.Author;

public interface AuthorDAO extends CrudDAO<Author, Integer>{

    @Override
    public Integer create(Author t) throws DAOException;

    @Override
    public boolean delete(Integer id) throws DAOException;

    @Override
    public Author read(Integer id) throws DAOException;

    @Override
    public boolean update(Author t) throws DAOException;

    
}
