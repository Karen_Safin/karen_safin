package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.property.AllNamesEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

public class TagDAOImpl implements TagDAO{

    private DataSource dataSource;

    public TagDAOImpl() {
    }

    public void setDataSource(DataSource dataSource){
        this.dataSource = dataSource;
    }

    /**
     *
     * @param tag
     * @return
     * @throws DAOException
     */
    @Override
    public Integer create(Tag tag) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Integer rezId = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO " 
                    + AllNamesEnum.TAG_TABLE_NAME.getValue()+" ("
                    + AllNamesEnum.TAG_ID.getValue() +", "
                    + AllNamesEnum.TAG_NAME.getValue() +") VALUES (TAG_SEQ.nextval, ?)",
                    new String[] {AllNamesEnum.TAG_ID.getValue()});
            preparedStatement.setString(1, tag.getTagName());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            rezId = resultSet.getInt(1);
        } catch (SQLException e) {
            throw new DAOException("DAOException in create() method: " + e.getMessage());
        } finally{
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in create() method");
            }
        }
        return rezId;
    }

    /**
     *
     * @param idTag
     * @return
     * @throws DAOException
     */
    @Override
    public Tag read(Integer idTag) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Tag rezTag = new Tag();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT "
                    + AllNamesEnum.TAG_ID.getValue() + ", "
                    + AllNamesEnum.TAG_NAME.getValue() + " FROM " 
                    + AllNamesEnum.TAG_TABLE_NAME.getValue() + " WHERE " 
                    + AllNamesEnum.TAG_ID.getValue() + " = ?");
            preparedStatement.setInt(1, idTag);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setTagId(resultSet.getInt(AllNamesEnum.TAG_ID.getValue()));
                tag.setTagName(resultSet.getString(AllNamesEnum.TAG_NAME.getValue()));
                rezTag = tag;
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in read() method: " + e.getMessage());
        } finally{
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in read() method");
            }
        }
        return rezTag;
    }

    /**
     *
     * @param tag
     * @return
     * @throws DAOException
     */
    @Override
    public boolean update(Tag tag) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countMod = 0;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE " 
                    + AllNamesEnum.TAG_TABLE_NAME.getValue() + " SET "
                    + AllNamesEnum.TAG_NAME.getValue() + "= ? WHERE "
                    + AllNamesEnum.TAG_ID.getValue() + "= ?");
            preparedStatement.setString(1, tag.getTagName());
            preparedStatement.setInt(2, tag.getTagId());
            countMod = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("DAOException in update() method" + e.getMessage());
        } finally{
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in update() method");
            }
        }
        return countMod == 1;
    }

    /**
     *
     * @param idTag
     * @return
     * @throws DAOException
     */
    @Override
    public boolean delete(Integer idTag) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countDel = 0;
        try {
            if (idTag != null) {
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("DELETE FROM " 
                        + AllNamesEnum.TAG_TABLE_NAME.getValue()
                        + " WHERE " + AllNamesEnum.TAG_ID.getValue() + " IN (?)");
                preparedStatement.setInt(1, idTag);
                countDel = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in delete() method: " + e.getMessage());
        } finally{
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in delete() method");
            }
        }
        return countDel == 1;
    }
    
    /**
     *
     * @param idNews
     * @return
     * @throws DAOException
     */
    @Override
    public List<Tag> readAllTagsOnNews(Integer idNews) throws DAOException{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Tag> rezTags = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT "
                    + AllNamesEnum.TAG_ID.getValue() + ", "
                    + AllNamesEnum.TAG_NAME.getValue() + " FROM " 
                    + AllNamesEnum.TAG_TABLE_NAME.getValue() + " WHERE " 
                    + AllNamesEnum.TAG_ID.getValue() + " IN (SELECT " 
                    + AllNamesEnum.TAG_ID.getValue() + " FROM "
                    + AllNamesEnum.NEWS_TAG_TABLE_NAME.getValue() + " WHERE " 
                    + AllNamesEnum.NEWS_ID.getValue() + " = ?)");
            preparedStatement.setInt(1, idNews);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setTagId(resultSet.getInt(AllNamesEnum.TAG_ID.getValue()));
                tag.setTagName(resultSet.getString(AllNamesEnum.TAG_NAME.getValue()));
                rezTags.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in readOnNews() method: " + e.getMessage());
        } finally{
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in readOnNews() method");
            }
        }
        return rezTags;
    }

    /**
     *
     * @param connection
     * @param statement
     * @param resultSet
     * @return
     * @throws DAOException
     */
    public boolean closeAll(Connection connection, 
                            Statement  statement, 
                            ResultSet  resultSet) throws DAOException{
        boolean isCloseOK = true;
        if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close resultSet (" + e.getMessage() + ") ");
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close statement (" + e.getMessage() + ") ");
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close connection (" + e.getMessage() + ") ");
                }
            }
        return isCloseOK;
    }
    
}
