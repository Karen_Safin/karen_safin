package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.property.AllNamesEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;

public class CommentDAOImpl implements CommentDAO{
    
    private DataSource dataSource;

    public CommentDAOImpl() {
    }

    public void setDataSource(DataSource dataSource){
        this.dataSource = dataSource;
    }

    /**
     *
     * @param comment
     * @return
     * @throws DAOException
     */
    @Override
    public Integer create(Comment comment) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Integer rezId = null;
        Date date = new Date(System.currentTimeMillis());
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO "
                    + AllNamesEnum.COMMENTS_TABLE_NAME.getValue()+" ("
                    + AllNamesEnum.COMMENTS_COMMENTS_ID.getValue() +", "
                    + AllNamesEnum.COMMENTS_TEXT.getValue() +", "
                    + AllNamesEnum.CREATION_COMMENTS_DATE.getValue() +", "
                    + AllNamesEnum.NEWS_ID.getValue() +") VALUES (COMMENT_SEQ.nextval, ?, ?, ?)",
                    new String[] {AllNamesEnum.COMMENTS_ID.getValue()});
            preparedStatement.setString(1, comment.getCommentText());
            preparedStatement.setTimestamp(2, new Timestamp(date.getTime()));
            preparedStatement.setInt(3, comment.getNewsId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            rezId = resultSet.getInt(1);
        } catch (SQLException e) {
            throw new DAOException("DAOException in create() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in create() method");
            }
        }
        return rezId;
    }

    /**
     *
     * @param idComment
     * @return
     * @throws DAOException
     */
    @Override
    public Comment read(Integer idComment) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Comment rezComment = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT "
                    + AllNamesEnum.COMMENTS_ID.getValue() + ", "
                    + AllNamesEnum.COMMENTS_TEXT.getValue() + ", "
                    + AllNamesEnum.CREATION_COMMENTS_DATE.getValue() + ", "
                    + AllNamesEnum.NEWS_ID.getValue() + " FROM " 
                    + AllNamesEnum.COMMENTS_TABLE_NAME.getValue() + " WHERE " 
                    + AllNamesEnum.COMMENTS_ID.getValue() + " = ?");
            preparedStatement.setInt(1, idComment);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Comment comment = new Comment();
                comment.setCommentId(resultSet.getInt(AllNamesEnum.COMMENTS_ID.getValue()));
                comment.setCommentText(resultSet.getString(AllNamesEnum.COMMENTS_TEXT.getValue()));
                comment.setCreationDate(resultSet.getDate(AllNamesEnum.CREATION_COMMENTS_DATE.getValue()));
                comment.setNewsId(resultSet.getInt(AllNamesEnum.NEWS_ID.getValue()));
                rezComment = comment;
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in read() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in read() method");
            }
        }
        return rezComment;
    }

    /**
     *
     * @param comment
     * @return
     * @throws DAOException
     */
    @Override
    public boolean update(Comment comment) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Date date = new Date(System.currentTimeMillis());
        int countMod = 0;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE " 
                    + AllNamesEnum.COMMENTS_TABLE_NAME.getValue() + " SET "
                    + AllNamesEnum.COMMENTS_TEXT.getValue() + "= ?, "
                    + AllNamesEnum.CREATION_COMMENTS_DATE.getValue() + "= ? WHERE "
                    + AllNamesEnum.COMMENTS_ID.getValue() + "= ?");
            preparedStatement.setString(1, comment.getCommentText());
            preparedStatement.setTimestamp(2, new Timestamp(date.getTime()));
            preparedStatement.setInt(3, comment.getCommentId());
            countMod = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("DAOException in update() method" + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in update() method");
            }
        }
        return countMod == 1;
    }

    /**
     *
     * @param idComment
     * @return
     * @throws DAOException
     */
    @Override
    public boolean delete(Integer idComment) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countDel = 0;
        try {
            if (idComment != null) {
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("DELETE FROM " 
                        + AllNamesEnum.COMMENTS_TABLE_NAME.getValue() + " WHERE " 
                        + AllNamesEnum.COMMENTS_ID.getValue() + " IN (?)");
                preparedStatement.setInt(1, idComment);
                countDel = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in delete() method: " + e.getMessage());
        }finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in delete() method");
            }
        }
        return countDel == 1;
    }
    
    /**
     *
     * @param idNews
     * @return
     * @throws DAOException
     */
    @Override
    public boolean deleteCommentsInNews(Integer idNews) throws DAOException{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countDel = 0;
        try {
            if (idNews != null) {
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("DELETE FROM " 
                        + AllNamesEnum.COMMENTS_TABLE_NAME.getValue()+ " WHERE " 
                        + AllNamesEnum.NEWS_ID.getValue() + " IN (?)");
                preparedStatement.setInt(1, idNews);
                countDel = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in deleteCommentsInNews() method: " + e.getMessage());
        }finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in deleteCommentsInNews() method");
            }
        }
        return countDel == 1;
    }
    
    /**
     *
     * @param idNews
     * @return
     * @throws DAOException
     */
    @Override
    public List<Comment> readOnNews(Integer idNews) throws DAOException{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Comment> rezComments = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT "
                    + AllNamesEnum.COMMENTS_ID.getValue() + ", "
                    + AllNamesEnum.COMMENTS_TEXT.getValue() + ", "
                    + AllNamesEnum.CREATION_COMMENTS_DATE.getValue() + ", "
                    + AllNamesEnum.NEWS_ID.getValue() + " FROM "
                    + AllNamesEnum.COMMENTS_TABLE_NAME.getValue() + " WHERE " 
                    + AllNamesEnum.NEWS_ID.getValue() + " = ?");
            preparedStatement.setInt(1, idNews);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Comment comment = new Comment();
                comment.setCommentId(resultSet.getInt(AllNamesEnum.COMMENTS_ID.getValue()));
                comment.setCommentText(resultSet.getString(AllNamesEnum.COMMENTS_TEXT.getValue()));
                comment.setCreationDate(resultSet.getDate(AllNamesEnum.CREATION_COMMENTS_DATE.getValue()));
                comment.setNewsId(resultSet.getInt(AllNamesEnum.NEWS_ID.getValue()));
                rezComments.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in readOnNews() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in readOnNews() method");
            }
        }
        return  rezComments;
    }
    
    /**
     *
     * @param connection
     * @param statement
     * @param resultSet
     * @return
     * @throws DAOException
     */
    public boolean closeAll(Connection connection, 
                            Statement  statement, 
                            ResultSet  resultSet) throws DAOException{
        boolean isCloseOK = true;
        if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close resultSet (" + e.getMessage() + ") ");
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close statement (" + e.getMessage() + ") ");
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close connection (" + e.getMessage() + ") ");
                }
            }
        return isCloseOK;
    }
}