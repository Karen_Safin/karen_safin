package com.epam.newsmanagement.dao.exception;

public class DAOException extends Exception{

    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }
    
}
