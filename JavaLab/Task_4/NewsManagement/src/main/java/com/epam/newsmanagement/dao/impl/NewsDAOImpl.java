package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.property.AllNamesEnum;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

public class NewsDAOImpl implements NewsDAO{

    private DataSource dataSource;

    public NewsDAOImpl() {
    }

    public void setDataSource(DataSource dataSource){
        this.dataSource = dataSource;
    }

    /**
     *
     * @param news
     * @return
     * @throws DAOException
     */
    @Override
    public Integer create(News news) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Integer rezId = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO " 
                    + AllNamesEnum.NEWS_TABLE_NAME.getValue()+" ("
                    + AllNamesEnum.NEWS_ID.getValue() +", "
                    + AllNamesEnum.SHORT_TEXT.getValue() +", "
                    + AllNamesEnum.FULL_TEXT.getValue() +", "
                    + AllNamesEnum.TITLE.getValue() +", "
                    + AllNamesEnum.CREATION_NEWS_DATE.getValue() +", "
                    + AllNamesEnum.MODIFICATION_DATE.getValue() +")" +
                    " VALUES (NEWS_SEQ.nextval, ?, ?, ?, ?, ?)", 
                    new String[] {AllNamesEnum.NEWS_ID.getValue()});
            preparedStatement.setString(1, news.getShortText());
            preparedStatement.setString(2, news.getFullText());
            preparedStatement.setString(3, news.getTitle());
            preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
            preparedStatement.setTimestamp(5, new Timestamp(news.getCreationDate().getTime()));
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            rezId = resultSet.getInt(1);
        } catch (SQLException e) {
            throw new DAOException("DAOException in create() method: " + e.getMessage());
        } finally{
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in create() method");
            }
        }
        return rezId;
    }

    /**
     *
     * @param idNews
     * @return
     * @throws DAOException
     */
    @Override
    public News read(Integer idNews) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        News rezNews = new News();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT "
                    + AllNamesEnum.NEWS_ID.getValue() +", "
                    + AllNamesEnum.SHORT_TEXT.getValue() + ", "
                    + AllNamesEnum.FULL_TEXT.getValue() + ", "
                    + AllNamesEnum.TITLE.getValue() + ", "
                    + AllNamesEnum.CREATION_NEWS_DATE.getValue() + ", "
                    + AllNamesEnum.MODIFICATION_DATE.getValue() +" FROM " 
                    + AllNamesEnum.NEWS_TABLE_NAME.getValue() + " WHERE " 
                    + AllNamesEnum.NEWS_ID.getValue() + " = ?");
            preparedStatement.setInt(1, idNews);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                News news = new News();
                news.setNewsId(resultSet.getInt(AllNamesEnum.NEWS_ID.getValue()));
                news.setShortText(resultSet.getString(AllNamesEnum.SHORT_TEXT.getValue()));
                news.setFullText(resultSet.getString(AllNamesEnum.FULL_TEXT.getValue()));
                news.setTitle(resultSet.getString(AllNamesEnum.TITLE.getValue()));
                news.setCreationDate(resultSet.getDate(AllNamesEnum.CREATION_NEWS_DATE.getValue()));
                news.setModificationDate(resultSet.getDate(AllNamesEnum.MODIFICATION_DATE.getValue()));
                rezNews = news;
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in read() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in read() method");
            }
        }
        return rezNews;
    }

    /**
     *
     * @param news
     * @return
     * @throws DAOException
     */
    @Override
    public boolean update(News news) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countMod = 0;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE " 
                    + AllNamesEnum.NEWS_TABLE_NAME.getValue() + " SET "
                    + AllNamesEnum.SHORT_TEXT.getValue() + "= ?, "
                    + AllNamesEnum.FULL_TEXT.getValue() + "= ?, "
                    + AllNamesEnum.TITLE.getValue() + "= ?, "
                    + AllNamesEnum.MODIFICATION_DATE.getValue() + "= ? WHERE "
                    + AllNamesEnum.NEWS_ID.getValue() + "= ?");
            preparedStatement.setString(1, news.getShortText());
            preparedStatement.setString(2, news.getFullText());
            preparedStatement.setString(3, news.getTitle());
            preparedStatement.setTimestamp(4, new Timestamp(news.getModificationDate().getTime()));
            preparedStatement.setInt(5, news.getNewsId());
            countMod = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("DAOException in update() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in update() method");
            }
        }
        return countMod == 1;
    }

    /**
     *
     * @param idNews
     * @return
     * @throws DAOException
     */
    @Override
    public boolean delete(Integer idNews) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countDel = 0;
        try {
            if (idNews != null) {
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("DELETE FROM " 
                        + AllNamesEnum.NEWS_TABLE_NAME.getValue() + " WHERE " 
                        + AllNamesEnum.NEWS_ID.getValue() + " IN (?)");
                preparedStatement.setInt(1, idNews);
                countDel = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in delete() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in delete() method");
            }
        }
        return countDel == 1;
    }    
    
    /**
     *
     * @return
     * @throws DAOException
     */
    @Override
    public List<News> selectAllNewses() throws DAOException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<News> rezNewses = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT "
                    + AllNamesEnum.NEWS_ID.getValue() +", "
                    + AllNamesEnum.SHORT_TEXT.getValue() + ", "
                    + AllNamesEnum.FULL_TEXT.getValue() + ", "
                    + AllNamesEnum.TITLE.getValue() + ", "
                    + AllNamesEnum.CREATION_NEWS_DATE.getValue() + ", "
                    + AllNamesEnum.MODIFICATION_DATE.getValue() +" FROM " 
                    + AllNamesEnum.NEWS_TABLE_NAME.getValue());
            while (resultSet.next()) {
                News news = new News();
                news.setNewsId(resultSet.getInt(AllNamesEnum.NEWS_ID.getValue()));
                news.setShortText(resultSet.getString(AllNamesEnum.SHORT_TEXT.getValue()));
                news.setFullText(resultSet.getString(AllNamesEnum.FULL_TEXT.getValue()));
                news.setTitle(resultSet.getString(AllNamesEnum.TITLE.getValue()));
                news.setCreationDate(resultSet.getDate(AllNamesEnum.CREATION_NEWS_DATE.getValue()));
                news.setModificationDate(resultSet.getDate(AllNamesEnum.MODIFICATION_DATE.getValue()));
                rezNewses.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in selectAllNewses() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, statement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in selectAllNewses() method");
            }
        }
        return rezNewses;
    }

    /**
     *
     * @param idAuthor
     * @return
     * @throws DAOException
     */
    @Override
    public List<News> selectByAuthor(Integer idAuthor) throws DAOException{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<News> rezNewses = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT "
                    + AllNamesEnum.NEWS_ID.getValue() +", "
                    + AllNamesEnum.SHORT_TEXT.getValue() + ", "
                    + AllNamesEnum.FULL_TEXT.getValue() + ", "
                    + AllNamesEnum.TITLE.getValue() + ", "
                    + AllNamesEnum.CREATION_NEWS_DATE.getValue() + ", "
                    + AllNamesEnum.MODIFICATION_DATE.getValue() +" FROM " 
                    + AllNamesEnum.NEWS_TABLE_NAME.getValue()+ " WHERE "
                    + AllNamesEnum.NEWS_ID.getValue() + " IN (SELECT "
                    + AllNamesEnum.NEWS_ID.getValue() + " FROM "
                    + AllNamesEnum.NEWS_AUTHOR_TABLE_NAME.getValue() + " WHERE " 
                    + AllNamesEnum.AUTHOR_ID.getValue() + " = ?)");
            preparedStatement.setInt(1, idAuthor);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                News news = new News();
                news.setNewsId(resultSet.getInt(AllNamesEnum.NEWS_ID.getValue()));
                news.setShortText(resultSet.getString(AllNamesEnum.SHORT_TEXT.getValue()));
                news.setFullText(resultSet.getString(AllNamesEnum.FULL_TEXT.getValue()));
                news.setTitle(resultSet.getString(AllNamesEnum.TITLE.getValue()));
                news.setCreationDate(resultSet.getDate(AllNamesEnum.CREATION_NEWS_DATE.getValue()));
                news.setModificationDate(resultSet.getDate(AllNamesEnum.MODIFICATION_DATE.getValue()));
                rezNewses.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in selectByAuthor() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in selectByAuthor() method");
            }
        }
        return rezNewses;
    }
    
    /**
     *
     * @param idTag
     * @return
     * @throws DAOException
     */
    @Override
    public List<News> selectByTag(Integer idTag) throws DAOException{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<News> rezNewses = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT "
                    + AllNamesEnum.NEWS_ID.getValue() +", "
                    + AllNamesEnum.SHORT_TEXT.getValue() + ", "
                    + AllNamesEnum.FULL_TEXT.getValue() + ", "
                    + AllNamesEnum.TITLE.getValue() + ", "
                    + AllNamesEnum.CREATION_NEWS_DATE.getValue() + ", "
                    + AllNamesEnum.MODIFICATION_DATE.getValue() +" FROM " 
                    + AllNamesEnum.NEWS_TABLE_NAME.getValue()+ " WHERE "
                    + AllNamesEnum.NEWS_ID.getValue() + " IN (SELECT "
                    + AllNamesEnum.NEWS_ID.getValue() + " FROM "
                    + AllNamesEnum.NEWS_TAG_TABLE_NAME.getValue() + " WHERE " 
                    + AllNamesEnum.TAG_ID.getValue() + " = ?)");
            preparedStatement.setInt(1, idTag);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                News news = new News();
                news.setNewsId(resultSet.getInt(AllNamesEnum.NEWS_ID.getValue()));
                news.setShortText(resultSet.getString(AllNamesEnum.SHORT_TEXT.getValue()));
                news.setFullText(resultSet.getString(AllNamesEnum.FULL_TEXT.getValue()));
                news.setTitle(resultSet.getString(AllNamesEnum.TITLE.getValue()));
                news.setCreationDate(resultSet.getDate(AllNamesEnum.CREATION_NEWS_DATE.getValue()));
                news.setModificationDate(resultSet.getDate(AllNamesEnum.MODIFICATION_DATE.getValue()));
                rezNewses.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in selectByTag() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, resultSet);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in selectByTag() method");
            }
        }
        return rezNewses;
    }
    
    /**
     *
     * @param idNews
     * @param idAuthor
     * @return
     * @throws DAOException
     */
    @Override
    public boolean addRelationForAuthorAndNews(Integer idNews, Integer idAuthor) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int count = 0;
        try {
            if(idNews != null && idAuthor != null){
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("INSERT INTO " 
                        + AllNamesEnum.NEWS_AUTHOR_TABLE_NAME.getValue()+" ("
                        + AllNamesEnum.NEWS_AUTHOR_ID.getValue() + ", "
                        + AllNamesEnum.NEWS_ID.getValue() +", "
                        + AllNamesEnum.AUTHOR_ID.getValue() +") " +
                        "VALUES (NEWS_AUTHOR_SEQ.nextval, ?, ?)");
                preparedStatement.setInt(1, idNews);
                preparedStatement.setInt(2, idAuthor);
                count = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in addRelation() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in addRelationForAuthorAndNews() method");
            }
        }
        return count == 1;
    }
    
    /**
     *
     * @param idNews
     * @param idTag
     * @return
     * @throws DAOException
     */
    @Override
    public boolean addRelationForTagAndNews(Integer idNews, Integer idTag) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int count = 0;
        try {
            if(idNews != null && idTag != null){
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("INSERT INTO " 
                        + AllNamesEnum.NEWS_TAG_TABLE_NAME.getValue()+" ("
                        + AllNamesEnum.NEWS_TAG_ID.getValue() + ", "
                        + AllNamesEnum.NEWS_ID.getValue() +", "
                        + AllNamesEnum.TAG_ID.getValue() +") "
                        + "VALUES (NEWS_TAG_SEQ.nextval, ?, ?)");
                preparedStatement.setInt(1, idNews);
                preparedStatement.setInt(2, idTag);
                count = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in addRelationRForTagAndNews() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in addRelationRForTagAndNews() method");
            }
        }
        return count == 1;
    }
    
    /**
     *
     * @param idAuthor
     * @return
     * @throws DAOException
     */
    @Override
    public boolean deleteRelationForAuthorAndNewsByAuthor(Integer idAuthor) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countDel = 0;
        try {
            if (idAuthor != null) {
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("DELETE FROM " 
                        + AllNamesEnum.NEWS_AUTHOR_TABLE_NAME.getValue() + " WHERE "
                        + AllNamesEnum.AUTHOR_ID.getValue() + " IN (?)");
                preparedStatement.setInt(1, idAuthor);
                countDel = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in delete() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in delete() method");
            }
        }
        return countDel == 1;
    }
    
    /**
     *
     * @param idTag
     * @return
     * @throws DAOException
     */
    @Override
    public boolean deleteRelationForTagAndNewsByTag(Integer idTag) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countDel = 0;
        try {
            if (idTag != null) {
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("DELETE FROM " 
                        + AllNamesEnum.NEWS_TAG_TABLE_NAME.getValue() + " WHERE "
                        + AllNamesEnum.TAG_ID.getValue() + " IN (?)");
                preparedStatement.setInt(1, idTag);
                countDel = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in delete() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in delete() method");
            }
        }
        return countDel == 1;
    }
       
    /**
     *
     * @param idNews
     * @return
     * @throws DAOException
     */
    @Override
    public boolean deleteRelationForAuthorAndNewsByNews(Integer idNews) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countDel = 0;
        try {
            if (idNews != null) {
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("DELETE FROM " 
                        + AllNamesEnum.NEWS_AUTHOR_TABLE_NAME.getValue() + " WHERE "
                        + AllNamesEnum.NEWS_ID.getValue() + " IN (?)");
                preparedStatement.setInt(1, idNews);
                countDel = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in delete() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in delete() method");
            }
        }
        return countDel == 1;
    }
    
    /**
     *
     * @param idNews
     * @return
     * @throws DAOException
     */
    @Override
    public boolean deleteRelationForTagAndNewsByNews(Integer idNews) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int countDel = 0;
        try {
            if (idNews != null) {
                connection = dataSource.getConnection();
                preparedStatement = connection.prepareStatement("DELETE FROM " 
                        + AllNamesEnum.NEWS_TAG_TABLE_NAME.getValue() + " WHERE "
                        + AllNamesEnum.NEWS_ID.getValue() + " IN (?)");
                preparedStatement.setInt(1, idNews);
                countDel = preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOException in delete() method: " + e.getMessage());
        } finally {
            try{
                closeAll(connection, preparedStatement, null);   
            } catch(DAOException e){
                throw new DAOException("DAOException at " + e.getMessage() + " in delete() method");
            }
        }
        return countDel == 1;
    }
    
    /**
     *
     * @param connection
     * @param statement
     * @param resultSet
     * @return
     * @throws DAOException
     */
    public boolean closeAll(Connection connection, 
                            Statement  statement, 
                            ResultSet  resultSet) throws DAOException{
        boolean isCloseOK = true;
        if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close resultSet (" + e.getMessage() + ") ");
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close statement (" + e.getMessage() + ") ");
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    isCloseOK = false;
                    throw new DAOException("close connection (" + e.getMessage() + ") ");
                }
            }
        return isCloseOK;
    }

}