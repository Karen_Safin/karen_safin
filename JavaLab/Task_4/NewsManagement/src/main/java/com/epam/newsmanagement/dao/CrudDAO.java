package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;

public interface CrudDAO <T, PK> {
    public PK create(T t) throws DAOException;
    public T read(PK id) throws DAOException;
    public boolean update(T t) throws DAOException;
    public boolean delete(PK id) throws DAOException;
}
