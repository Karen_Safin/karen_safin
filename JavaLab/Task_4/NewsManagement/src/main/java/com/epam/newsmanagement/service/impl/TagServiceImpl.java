package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dao.impl.TagDAOImpl;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import java.sql.SQLException;
import java.util.List;

public class TagServiceImpl implements TagService{

    private TagDAOImpl tagDao;

    public TagServiceImpl(TagDAOImpl tagDAO) throws SQLException {
        this.tagDao = tagDAO;
    }

    /**
     *
     * @param tagName
     * @return
     * @throws ServiceException
     */
    @Override
    public int addTag(String tagName) throws ServiceException {
        Tag tag = new Tag(tagName);
        try {
            return tagDao.create(tag);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idTag
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean deleteTag(int idTag) throws ServiceException {
        try {
            return tagDao.delete(idTag);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idTag
     * @param tagName
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean editTag(int idTag, String tagName) throws ServiceException {
        Tag tag = new Tag(idTag, tagName);
        try {
            return tagDao.create(tag) >= 1;
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idTag
     * @return
     * @throws ServiceException
     */
    @Override
    public Tag getTagByID(int idTag) throws ServiceException {
        Tag tag = null;
        try {
            tag =  tagDao.read(idTag);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
        return tag;
    }

    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Override
    public List<Tag> getAllTagsOnNews(int idNews) throws ServiceException {
        List<Tag> tags = null;
        try {
            tags = tagDao.readAllTagsOnNews(idNews);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
        return tags;
    }

}
