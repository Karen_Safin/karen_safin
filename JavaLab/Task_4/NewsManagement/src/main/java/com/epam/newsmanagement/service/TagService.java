package com.epam.newsmanagement.service;

import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;
import java.util.List;

public interface TagService {
    public int addTag(String tag) throws ServiceException;
    public boolean deleteTag(int idTag) throws ServiceException;
    public boolean editTag(int idTag, String tag) throws ServiceException;
    public Tag getTagByID(int idTag) throws ServiceException;
    public List<Tag> getAllTagsOnNews(int idNews) throws ServiceException;
}
