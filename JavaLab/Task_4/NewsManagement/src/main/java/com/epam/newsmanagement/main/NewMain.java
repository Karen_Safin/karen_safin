package com.epam.newsmanagement.main;


import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.impl.MainServiceImpl;
import java.sql.Date;
import java.sql.SQLException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class NewMain {
    
    private final static ApplicationContext aplicationContext = new ClassPathXmlApplicationContext("SpringXMLConfig.xml");
    
    public static void main(String[] args) throws SQLException, ServiceException {
        MainServiceImpl mainService = (MainServiceImpl) aplicationContext.getBean("mainService");
        //mainService.addAuthorByName(1,"Karen");
        //Author author = (Author) mainService.getAuthor(1);
        //mainService.editAuthor(1, "qqqqqqqqqqq");
        //System.out.println(mainService.deleteAuthor(2));
        //mainService.addComments(1, "wwwwwwwww", new Date(System.currentTimeMillis()));
        System.out.println(mainService.getNewsesByAuthor(1).get(0).getTitle());
    }
    
}
