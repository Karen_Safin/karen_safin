package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dao.impl.NewsDAOImpl;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.exception.ServiceException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class NewsServiceImpl implements NewsService{

    private NewsDAOImpl newsDAO;

    public NewsServiceImpl(NewsDAOImpl newsDAO) throws SQLException {
        this.newsDAO = newsDAO;
    }

    /**
     *
     * @param shortText
     * @param fullText
     * @param title
     * @param creationDate
     * @return
     * @throws ServiceException
     */
    @Override
    public int addNews(String shortText, String fullText, String title, Date creationDate) throws ServiceException {
        News news = new News(shortText, fullText, title, creationDate);
        try {
            return newsDAO.create(news);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idNews
     * @param shortText
     * @param fullText
     * @param title
     * @param modificationDate
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean editNews(int idNews, String shortText, String fullText, String title, Date modificationDate) throws ServiceException {
        News news = new News(idNews, shortText, fullText, title, modificationDate);
        try {
            return newsDAO.update(news);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean deleteNews(int idNews) throws ServiceException {
        try {
            return newsDAO.delete(idNews);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Override
    public News getNews(int idNews) throws ServiceException {
        News news = null;
        try {
            news = newsDAO.read(idNews);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
        return news;
    }

    /**
     *
     * @return
     * @throws ServiceException
     */
    @Override
    public List<News> getAllNewses() throws ServiceException {
        List<News> newses = null;
        try {
            newses = newsDAO.selectAllNewses();
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
        return newses;
    }

    /**
     *
     * @param idAuthor
     * @return
     * @throws ServiceException
     */
    @Override
    public List<News> getNewsesByAuthor(int idAuthor) throws ServiceException {
        List<News> newses = null;
        try {
            newses = newsDAO.selectByAuthor(idAuthor);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
        return newses;
    }

    /**
     *
     * @param idTag
     * @return
     * @throws ServiceException
     */
    @Override
    public List<News> getNewsesByTag(int idTag) throws ServiceException {
        List<News> newses = null;
        try {
            newses = newsDAO.selectByTag(idTag);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
        return newses;
    }

    /**
     *
     * @param idNews
     * @param idAuthor
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean addRelationForAuthorAndNews(Integer idNews, Integer idAuthor) throws ServiceException {
        try {
            return newsDAO.addRelationForAuthorAndNews(idNews, idAuthor);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idNews
     * @param idTag
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean addRelationForTagAndNews(Integer idNews, Integer idTag) throws ServiceException {
        try {
            return newsDAO.addRelationForTagAndNews(idNews, idTag);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idAuthor
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean deleteRelationForAuthorAndNewsByAuthor(Integer idAuthor) throws ServiceException {
        try {
            return newsDAO.deleteRelationForAuthorAndNewsByAuthor(idAuthor);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idTag
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean deleteRelationForTagAndNewsByTag(Integer idTag) throws ServiceException {
        try {
            return newsDAO.deleteRelationForTagAndNewsByTag(idTag);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }
    
    /**
     *
     * @param idAuthor
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean deleteRelationForAuthorAndNewsByNews(Integer idAuthor) throws ServiceException {
        try {
            return newsDAO.deleteRelationForAuthorAndNewsByNews(idAuthor);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idTag
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean deleteRelationForTagAndNewsByNews(Integer idTag) throws ServiceException {
        try {
            return newsDAO.deleteRelationForTagAndNewsByNews(idTag);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }
}
