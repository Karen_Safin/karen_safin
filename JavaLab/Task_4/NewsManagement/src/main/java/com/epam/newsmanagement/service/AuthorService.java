package com.epam.newsmanagement.service;

import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface AuthorService {
    public int addAuthorByName(String nameOfAuthor) throws ServiceException;
    public boolean deleteAuthor(int idAuthor) throws ServiceException;
    public boolean editAuthor(int idAuthor, String nameOfAuthor) throws ServiceException;
    public Author getAuthor(int idNews) throws ServiceException;
}
