package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dao.impl.CommentDAOImpl;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.exception.ServiceException;
import java.sql.SQLException;
import java.util.List;

public class CommentServiceImpl implements CommentService{

    private CommentDAOImpl commentDAO;

    public CommentServiceImpl(CommentDAOImpl commentDAO) throws SQLException {
        this.commentDAO = commentDAO;
    }

    /**
     *
     * @param idNews
     * @param commentText
     * @return
     * @throws ServiceException
     */
    @Override
    public int addComments(int idNews, String commentText) throws ServiceException {
        Comment comment = new Comment(commentText, idNews);
        try {
            return commentDAO.create(comment);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idComment
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean deleteComments(int idComment) throws ServiceException {
        try {
            return commentDAO.delete(idComment);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean deleteCommentsInNews(int idNews) throws ServiceException {
        try {
            return commentDAO.deleteCommentsInNews(idNews);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idComment
     * @param commentText
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean editComments(int idComment, String commentText) throws ServiceException {
        Comment comment = new Comment(idComment, commentText);
        try {
            return commentDAO.update(comment);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     *
     * @param idNews
     * @return
     * @throws ServiceException
     */
    @Override
    public List<Comment> getComments(int idNews) throws ServiceException {
        try {
            return commentDAO.readOnNews(idNews);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

}
