package com.epam.newsmanagement.service;

import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.service.exception.ServiceException;
import java.util.List;

public interface CommentService {
    public int addComments(int idNews, String commentText) throws ServiceException;
    public boolean deleteComments(int idComment) throws ServiceException;
    public boolean deleteCommentsInNews(int idNews) throws ServiceException;
    public boolean editComments(int idComment, String commentText) throws ServiceException;
    public List<Comment> getComments(int idNews) throws ServiceException;
}
