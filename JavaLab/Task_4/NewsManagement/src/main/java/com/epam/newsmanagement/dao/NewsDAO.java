package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.model.News;
import java.util.List;

public interface NewsDAO extends CrudDAO<News, Integer>{

    @Override
    public boolean delete(Integer id) throws DAOException;
    @Override
    public boolean update(News t) throws DAOException;
    @Override
    public News read(Integer id) throws DAOException;
    @Override
    public Integer create(News t) throws DAOException;
    
    public List<News> selectAllNewses() throws DAOException;
    public List<News> selectByAuthor(Integer idAuthor) throws DAOException;
    public List<News> selectByTag(Integer idTag) throws DAOException;
    public boolean addRelationForAuthorAndNews(Integer idNews, Integer idAuthor) throws DAOException;
    public boolean addRelationForTagAndNews(Integer idNews, Integer idTag) throws DAOException;
    public boolean deleteRelationForAuthorAndNewsByAuthor(Integer idAuthor) throws DAOException;
    public boolean deleteRelationForTagAndNewsByTag(Integer idTag) throws DAOException;
    public boolean deleteRelationForAuthorAndNewsByNews(Integer idNews) throws DAOException;
    public boolean deleteRelationForTagAndNewsByNews(Integer idNews) throws DAOException;
}
