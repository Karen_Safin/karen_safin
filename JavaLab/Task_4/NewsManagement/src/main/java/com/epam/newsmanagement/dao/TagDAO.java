package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.model.Tag;
import java.util.List;

public interface TagDAO extends CrudDAO<Tag, Integer>{

    @Override
    public boolean delete(Integer id) throws DAOException;
    @Override
    public boolean update(Tag t) throws DAOException;
    @Override
    public Tag read(Integer id) throws DAOException;
    @Override
    public Integer create(Tag t) throws DAOException;
    
    public List<Tag> readAllTagsOnNews(Integer idNews) throws DAOException;
}
