<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0" xmlns:validate="xalan://com.epam.task.validation.GoodsValidator"
	extension-element-prefixes="validate">
    <xsl:output method="xml"/>

    <xsl:param name="categoryName" />
    <xsl:param name="subcategoryName" />
    <xsl:param name="producer" />
    <xsl:param name="model" />
    <xsl:param name="dateOfIssue" />
    <xsl:param name="color" />
    <xsl:param name="price" />

    <xsl:template match="@*|node()" name ="save">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template
		match="/products/category[@name=$categoryName]/subcategory[@name=$subcategoryName]">
        <xsl:copy>
            <xsl:attribute name="name">
                <xsl:value-of select="@name" />
            </xsl:attribute>
            <xsl:apply-templates select="*" />
                    <xsl:element name="goods">
                        <xsl:element name="producer">
                            <xsl:value-of select="$producer" />
                        </xsl:element>
                        <xsl:element name="model">
                            <xsl:value-of select="$model" />
                        </xsl:element>
                        <xsl:element name="date-of-issue">
                            <xsl:value-of select="$dateOfIssue" />
                        </xsl:element>
                        <xsl:element name="color">
                            <xsl:value-of select="$color" />
                        </xsl:element>
                        <xsl:choose>
                            <xsl:when test="$price">
                                <xsl:element name="price">
                                    <xsl:value-of select="$price" />
                                </xsl:element>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:element name="not-in-stock"></xsl:element>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:element>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>