package com.epam.xsltask.commandfactory;

import com.epam.xsltask.command.Command;
import com.epam.xsltask.command.NoCommand;
import com.epam.xsltask.command.CategoryCommand;
import com.epam.xsltask.command.SubcategoryCommand;
import com.epam.xsltask.command.AddProductCommand;
import com.epam.xsltask.command.SaveProductCommand;

import java.util.EnumMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class CommandFactory {

    private static Logger log = Logger.getLogger(CommandFactory.class);
    private static final String PARAM_COMMAND = "command";

    enum CommandEnum {

        CATEGORY, SUBCATEGORY, ADDPRODUCT, SAVEPRODUCT;
    }
    private EnumMap<CommandEnum, Command> commandEnumMap;
    private static CommandFactory instance = new CommandFactory();

    private CommandFactory() {
        init();
    }

    private void init() {

        commandEnumMap = new EnumMap(CommandEnum.class);

        for (CommandEnum command : CommandEnum.values()) {
            switch (command) {
                case CATEGORY:
                    commandEnumMap.put(command, new CategoryCommand());
                    break;
                case SUBCATEGORY:
                    commandEnumMap.put(command, new SubcategoryCommand());
                    break;
                case ADDPRODUCT:
                    commandEnumMap.put(command, new AddProductCommand());
                    break;
                case SAVEPRODUCT:
                    commandEnumMap.put(command, new SaveProductCommand());
                    break;
            }
        }
    }

    public static CommandFactory getInstance() {
        return instance;
    }

    public Command getCommand(HttpServletRequest request) {
        // fetch commands from the query
        String action = request.getParameter(PARAM_COMMAND);
        //checking for the team
        if (action == null) {
            return new NoCommand();
        }
        // definition of key
        CommandEnum commandEnum = CommandEnum.valueOf(action.toUpperCase());
        if (commandEnumMap.containsKey(commandEnum)) {
            return commandEnumMap.get(commandEnum);
        }
        return new NoCommand();
    }
}