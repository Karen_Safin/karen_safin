<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" />

    <xsl:template match="/">
        <html>
            <head>
                <title>Shop</title>
                <link rel="stylesheet" type="text/css" href="css/style.css"/>
            </head>
            <body>
                
                <table border="0" cellpadding="6" align="center">
                List of Categories
                    <hr/>
                    <xsl:for-each select="/products/category">
                        <xsl:call-template name="category-print" />
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="category-print">
        <tr>
            <td>
                <xsl:param name="category-name" select="@name"></xsl:param>
                <a href="controller?command=category&amp;categoryName={$category-name}">
                    <xsl:value-of select="@name" />
                    (
                    <xsl:value-of select="count(subcategory/goods)" />
                    )
                </a>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>