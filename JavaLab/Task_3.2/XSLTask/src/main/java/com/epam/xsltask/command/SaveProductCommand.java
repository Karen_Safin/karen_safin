package com.epam.xsltask.command;

import com.epam.xsltask.entity.Goods;
import com.epam.xsltask.enums.TaskEnum;
import com.epam.xsltask.transformer.XslTransformer;
import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SaveProductCommand implements Command {

    private static final String URL_PATH1 = "controller?command=subcategory&categoryName=";
    private static final String URL_PATH2 = "&subcategoryName=";

    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            StringBuilder pathUrl = new StringBuilder(URL_PATH1);
            Writer wr = response.getWriter();
            XslTransformer tr = XslTransformer.getInstance();
            String categoryName = request.getParameter(TaskEnum.PARAM_CATEGORY_NAME.getValue());
            String subcategoryName = request.getParameter(TaskEnum.PARAM_SUBCATEGORY_NAME.getValue());
            Goods goods = new Goods();
            goods.setProducer(request.getParameter(TaskEnum.PRODUCER.getValue()));
            goods.setModel(request.getParameter(TaskEnum.MODEL.getValue()));
            goods.setDateOfIssue(request.getParameter(TaskEnum.DATE_OF_ISSUE.getValue()));
            goods.setColor(request.getParameter(TaskEnum.COLOR.getValue()));
            goods.setPrice(request.getParameter(TaskEnum.PRICE.getValue()));
            if (tr.rewrite(categoryName, subcategoryName, goods, wr)) {
                pathUrl.append(categoryName);
                pathUrl.append(URL_PATH2);
                pathUrl.append(subcategoryName);
                response.sendRedirect(pathUrl.toString());

            }
        } catch (IOException ex) {
            Logger.getLogger(SaveProductCommand.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
}