package com.epam.xsltask.enums;

public enum TaskEnum {
    PRODUCTS("products"),
    CATEGORY("category"),
    NAME("name"),
    SUBCATEGORY("subcategory"),
    GOODS("goods"),
    PRODUCER("producer"),
    MODEL("model"),
    COLOR("color"),
    PRICE("price"),
    DATE_OF_ISSUE("dateOfIssue"),
    PATH_FILE_XML(""),
    PATH_FILE_XSL_CATEGORY(""),
    PATH_FILE_XSL_SUBCATEGORY(""),
    PATH_FILE_XSL_GOODS(""),
    PATH_FILE_XSL_ADD_PRODUCT(""),
    PATH_FILE_XSL_SAVE_PRODUCT(""),

    PARAM_CATEGORY_NAME("categoryName"),
    PARAM_SUBCATEGORY_NAME("subcategoryName"),
    DATE_FORMAT("dd-mm-yyyy");
    private String value;

    private TaskEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}