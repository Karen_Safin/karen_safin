package com.epam.xsltask.controller;

import com.epam.xsltask.command.Command;
import com.epam.xsltask.commandfactory.CommandFactory;
import com.epam.xsltask.enums.TaskEnum;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

public class Controller extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final String ENCODING = "utf-8";
    private static final String PATH_XML = "WEB-INF/classes/com/epam/xsltask/resource/products.xml";
    private static final String PATH_XSL_CATEGORY = "WEB-INF/classes/com/epam/xsltask/xsl/category.xsl";
    private static final String PATH_XSL_SUBCATEGORY = "WEB-INF/classes/com/epam/xsltask/xsl/subcategory.xsl";
    private static final String PATH_XSL_GOODS = "WEB-INF/classes/com/epam/xsltask/xsl/goods.xsl";
    private static final String PATH_XSL_ADD_PRODUCT = "WEB-INF/classes/com/epam/xsltask/xsl/addGoods.xsl";
    private static final String PATH_XSL_SAVE_PRODUCT = "WEB-INF/classes/com/epam/xsltask/xsl/validation.xsl";
    private static Logger log = Logger.getLogger(Controller.class);

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletConfig().getServletContext();
        String path_file = servletContext.getRealPath(PATH_XML);
        TaskEnum.PATH_FILE_XML.setValue(path_file);
        path_file = servletContext.getRealPath(PATH_XSL_CATEGORY);
        TaskEnum.PATH_FILE_XSL_CATEGORY.setValue(path_file);
        path_file = servletContext.getRealPath(PATH_XSL_SUBCATEGORY);
        TaskEnum.PATH_FILE_XSL_SUBCATEGORY.setValue(path_file);

        path_file = servletContext.getRealPath(PATH_XSL_GOODS);
        TaskEnum.PATH_FILE_XSL_GOODS.setValue(path_file);
        path_file = servletContext.getRealPath(PATH_XSL_ADD_PRODUCT);
        TaskEnum.PATH_FILE_XSL_ADD_PRODUCT.setValue(path_file);

        path_file = servletContext.getRealPath(PATH_XSL_SAVE_PRODUCT);
        TaskEnum.PATH_FILE_XSL_SAVE_PRODUCT.setValue(path_file);
        super.init();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding(ENCODING);
        response.setCharacterEncoding(ENCODING);
        Command command = CommandFactory.getInstance().getCommand(request);
        command.execute(request,response);
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}