<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
    <xsl:output method="html" />
    <xsl:param name="categoryName" />
    <xsl:param name="subcategoryName" />
    <xsl:param name="producer" />
    <xsl:param name="model" />
    <xsl:param name="dateOfIssue" />
    <xsl:param name="color" />
    <xsl:param name="price" />

    <xsl:template match="/">
        <html>
            <head>
                <title>Shop</title>
                <link rel="stylesheet" type="text/css" href="css/style.css"/>
            </head>
            <body>
                <xsl:call-template name="addForm" />
            </body>
        </html>
    </xsl:template>

    <xsl:template name="addForm">
        <form name="addProduct" action="controller" method="post">
            <table border="0" cellpadding="6" align="center">
                Add Product in category: 
                <xsl:value-of select="$categoryName"/> and Subcategory:
                <xsl:value-of select="$subcategoryName" />
                <hr/>
                <tr bgcolor="grey">
                    <td>Producer</td>
                    <td>Model</td>
                    <td>Date Of Issue</td>
                    <td>Color</td>
                    <td>Price</td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="producer" size="10" value="{$producer}"/>
                    </td>
                    <td>
                        <input type="text" name="model" size="4" value="{$model}"></input>
                    </td>
                    <td>
                        <input type="text" name="dateOfIssue" size="10" value="{$dateOfIssue}"></input>
                    </td>
                    <td>
                        <input type="text" name="color" size="10" value="{$color}"></input>
                    </td>
                    <td>
                        <input type="text" name="price" size="10" value="{$price}"></input>
                    </td>
                </tr>
            </table>
            <br/>
            <div align="center">
                <input type="hidden" name="command" value="saveProduct"/>
                <input type="hidden" name="categoryName" value="{$categoryName}" />
                <input type="hidden" name="subcategoryName" value="{$subcategoryName}" />
                <input type="submit" name="SAVE" value="SAVE"></input>
                <input type="button" onclick="history.back()" value="Back"></input>
            </div>
        </form>
    </xsl:template>
</xsl:stylesheet>