package com.epam.xsltask.transformer;

import com.epam.xsltask.entity.Goods;
import com.epam.xsltask.enums.TaskEnum;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.log4j.Logger;

public class XslTransformer {

    private static final Logger log = Logger.getLogger(XslTransformer.class);
    private static final String PARAM_NAME_ERROR = "error";
    private TransformerFactory factory;
    private static final XslTransformer instance = new XslTransformer();
    private ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();

    private XslTransformer() {
        this.factory = TransformerFactory.newInstance();
    }

    public static XslTransformer getInstance() {
        return instance;
    }

    public void transform(String xsl, String categoryName, String subcategoryName,
            Goods goods, Writer out) {
        try {

            Transformer transformer = factory.newTransformer(new StreamSource(xsl));
            if (categoryName != null) {
                transformer.setParameter(TaskEnum.PARAM_CATEGORY_NAME.getValue(), categoryName);
            }
            if (subcategoryName != null) {
                transformer.setParameter(TaskEnum.PARAM_SUBCATEGORY_NAME.getValue(), subcategoryName);
            }
            if (goods != null) {
                transformer.setParameter(TaskEnum.PRODUCER.getValue(), goods.getProducer());
                transformer.setParameter(TaskEnum.MODEL.getValue(), goods.getModel());
                transformer.setParameter(TaskEnum.DATE_OF_ISSUE.getValue(), goods.getDateOfIssue());
                transformer.setParameter(TaskEnum.COLOR.getValue(), goods.getColor());
                transformer.setParameter(TaskEnum.PRICE.getValue(), goods.getPrice());
            }
            transformer.transform(new StreamSource(TaskEnum.PATH_FILE_XML.getValue()),
                    new StreamResult(out));
        } catch (TransformerException e) {
            log.error(e);
        }
    }

    public boolean rewrite(String categoryName, String subcategoryName,
            Goods goods, Writer out) {
        FileWriter fw = null;
        Writer tmpOut = new StringWriter();
        boolean fl = false;
        rwl.writeLock().lock();
        try {
            Transformer transformer = factory.newTransformer(new StreamSource(
                    TaskEnum.PATH_FILE_XSL_SAVE_PRODUCT.getValue()));
            if (categoryName != null) {
                transformer.setParameter(TaskEnum.PARAM_CATEGORY_NAME.getValue(), categoryName);
            }
            if (subcategoryName != null) {
                transformer.setParameter(TaskEnum.PARAM_SUBCATEGORY_NAME.getValue(), subcategoryName);
            }
            if (goods != null) {
                transformer.setParameter(TaskEnum.PRODUCER.getValue(), goods.getProducer());
                transformer.setParameter(TaskEnum.MODEL.getValue(), goods.getModel());
                transformer.setParameter(TaskEnum.DATE_OF_ISSUE.getValue(), goods.getDateOfIssue());
                transformer.setParameter(TaskEnum.COLOR.getValue(), goods.getColor());
                transformer.setParameter(TaskEnum.PRICE.getValue(), goods.getPrice());
            }

            File xmlFile = new File(TaskEnum.PATH_FILE_XML.getValue());
            Source xmlSource = new StreamSource(xmlFile);

            transformer.transform(xmlSource, new StreamResult(tmpOut));
            if (transformer.getParameter(PARAM_NAME_ERROR) != null) {
                out.write(tmpOut.toString());
            } else {
                fw = new FileWriter(xmlFile);
                fw.write(tmpOut.toString());

                fl = true;
            }
        } catch (Exception e) {
            log.error(e);
            fl = false;
        } finally {
            rwl.writeLock().unlock();
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException e) {
                log.error(e);
                fl = false;
            }
        }
        return fl;
    }
}