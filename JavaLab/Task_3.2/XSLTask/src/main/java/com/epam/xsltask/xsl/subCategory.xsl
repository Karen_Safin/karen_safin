<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" />
    <xsl:param name="categoryName"></xsl:param>

    <xsl:template match="/" >
        <html>
            <head>
                <title>Shop</title>
                <link rel="stylesheet" type="text/css" href="css/style.css"/>
            </head>
            <body>
                
                <table border="0" cellpadding="6" align="center">
                Sucategory
                    <hr/>
                    <xsl:for-each select="/products/category[@name = $categoryName]/subcategory">
                        <xsl:call-template name="print-subcategory" />
                    </xsl:for-each>
                </table>
                <br/>
                <div align="center">
                    <input type="button" onclick="history.back()" value="Back"></input>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="print-subcategory">
        <tr>
            <td>
                <xsl:param name="subcategoryName" select="@name"></xsl:param>
                <a href="controller?command=subcategory&amp;categoryName={$categoryName}&amp;subcategoryName={$subcategoryName}">
                    <xsl:value-of select="@name" />
		(
                    <xsl:value-of select="count(goods)" />)
                </a>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>