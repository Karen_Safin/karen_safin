<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" />
    <xsl:param name="categoryName"></xsl:param>
    <xsl:param name="subcategoryName"></xsl:param>

    <xsl:template match="/" >
        <html>
            <head>
                <title>Shop</title>
                <link rel="stylesheet" type="text/css" href="css/style.css"/>
            </head>
            <body>
                <form name="addProduct" action="controller" method="post">
                    <table border="0" cellpadding="6" align="center">
                Goods
                        <hr/>
                        <tr bgcolor="grey">
                            <td>Producer</td>
                            <td>Model</td>
                            <td>Date Of Issue</td>
                            <td>Color</td>
                            <td>Price</td>
                        </tr>
                        <xsl:for-each
			select="/products/category[@name = $categoryName]/subcategory[@name = $subcategoryName]/goods">
                            <xsl:call-template name="print-goods" />
                        </xsl:for-each>
                    </table>
                    <br/>
                    <div align="center">
                        <input type="hidden" name="command" value="addProduct"/>
                        <input type="hidden" name="categoryName" value="{$categoryName}"/>
                        <input type="hidden" name="subcategoryName" value="{$subcategoryName}"/>
                        <input type="submit" name="add" value="Add"></input>
                        <input type="button" onclick="history.back()"  value="Back"></input>
                    </div>
                </form>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="print-goods">
        <tr>
            <td>
                <xsl:value-of select="producer" />
            </td>
            <td>
                <xsl:value-of select="model" />
            </td>
            <td>
                <xsl:value-of select="date-of-issue" />
            </td>
            <td>
                <xsl:value-of select="color" />
            </td>
            <td>
                <xsl:choose>
                    <xsl:when test="price">
                        <xsl:value-of select="price" />
                    </xsl:when>
                    <xsl:otherwise>
						Not in stock
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>