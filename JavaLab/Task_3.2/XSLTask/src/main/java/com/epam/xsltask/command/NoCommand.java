package com.epam.xsltask.command;

import com.epam.xsltask.enums.TaskEnum;
import com.epam.xsltask.transformer.XslTransformer;
import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NoCommand implements Command {

    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            Writer wr = response.getWriter();
            XslTransformer tr = XslTransformer.getInstance();
            tr.transform(TaskEnum.PATH_FILE_XSL_CATEGORY.getValue(), null, null, null, wr);
        } catch (IOException ex) {
            Logger.getLogger(NoCommand.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
}