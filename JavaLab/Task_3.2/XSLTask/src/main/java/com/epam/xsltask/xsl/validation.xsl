<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:vld="xalan://com.epam.task.validation.GoodsValidator"
                xmlns:exsl="http://exslt.org/common"
                version="1.0">
    <xsl:import href="addGoods.xsl" />
    <xsl:import href="saveGoods.xsl" />

    <xsl:param name="categoryName" />
    <xsl:param name="subcategoryName" />
    <xsl:param name="producer" />
    <xsl:param name="model" />
    <xsl:param name="dateOfIssue" />
    <xsl:param name="color" />
    <xsl:param name="price" />

    <xsl:variable name="Error">
        <xsl:if test="vld:isNotEmpty($producer) != 'true'">
            <error ms="Producer is required."/>
        </xsl:if>
        <xsl:if test="vld:isCorrectModel($model) != 'true'">
            <error ms="Model is not valid. It should consist of 2 letters and 3 digits."/>
        </xsl:if>
        
        <xsl:if test="vld:isCorrectDate($dateOfIssue) != 'true'">
            <error ms="Date is not valid. Right date format is dd-MM-yyyy."/>
        </xsl:if>
        <xsl:if test="vld:isNotEmpty($color) != 'true'">
            <error ms="Color includes only letters."/>
        </xsl:if>
        <xsl:if test="vld:isCorrectPrice($price) != 'true'">
            <error ms="Price is not valid."/>
        </xsl:if>
    </xsl:variable>

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="count(exsl:node-set($Error)/error) != 0">
                <xsl:param name="error" select="true" />
                <html>
                    <head>
                        <title>Shop/Add product</title>
                        <link rel="stylesheet" type="text/css" href="css/style.css"/>
                    </head>
                    <body>
                        <xsl:call-template name="addForm"/>
                        <div id="message_error">
                            <ul>
                                <xsl:for-each select="exsl:node-set($Error)/error">
                                    <li>
                                        <xsl:value-of select="@ms"/>
                                    </li>
                                </xsl:for-each>
                            </ul>
                        </div>
                    </body>
                </html>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="save" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>