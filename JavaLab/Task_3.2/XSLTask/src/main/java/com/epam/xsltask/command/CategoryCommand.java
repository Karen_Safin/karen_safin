package com.epam.xsltask.command;

import com.epam.xsltask.enums.TaskEnum;
import com.epam.xsltask.transformer.XslTransformer;
import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CategoryCommand implements Command {

    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            Writer wr = response.getWriter();
            XslTransformer tr = XslTransformer.getInstance();
            String categoryName = request.getParameter(TaskEnum.PARAM_CATEGORY_NAME.getValue());
            String subcategoryName = request.getParameter(TaskEnum.PARAM_SUBCATEGORY_NAME.getValue());
            tr.transform(TaskEnum.PATH_FILE_XSL_SUBCATEGORY.getValue(),
                    categoryName, subcategoryName, null, wr);
        } catch (IOException ex) {
            Logger.getLogger(CategoryCommand.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
}