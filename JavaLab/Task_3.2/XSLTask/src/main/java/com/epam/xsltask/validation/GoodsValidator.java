package com.epam.xsltask.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoodsValidator {

    private static Pattern model = Pattern.compile("([a-zA-Z]{2})(\\d{3})");
    private static Pattern date = Pattern.compile("((0[1-9])|([1-2][0-9])|(3[0-1]))-((0[1-9])|(1[0-2]))-([1-2][0-9]{3})");
    private static Pattern price = Pattern.compile("(\\d*)");

    public static boolean isCorrectModel(String smodel) {
        Matcher m = model.matcher(smodel);
        return m.matches();
    }

    public static boolean isCorrectDate(String sdate) {
        Matcher m = date.matcher(sdate);
        return m.matches();
    }

    public static boolean isCorrectPrice(String sprice) {
        Matcher m = price.matcher(sprice);
        return m.matches();
    }

    public static boolean isNotEmpty(String producer) {
        if (producer.isEmpty()) {
            return false;
        }
        return true;
    }
}