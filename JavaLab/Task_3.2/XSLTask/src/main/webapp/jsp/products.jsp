<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View products</title>
        <link type="text/css" href="css/style.css" rel="stylesheet" />
    </head>
    <body>
        <table width="100%" align="center">
            <tr >
                <th align="left"><div align="left">Category</div><div align="center">Subcategory</div><div align="right">Product Name</div></th>
                <th align="center">Model</th>
                <th align="center">date of issue</th>
                <th align="center">color</th>
                <th align="center">price</th>
            </tr>
            <c:forEach var="category" items="${Categorys}">
                <tr>
                    <td align="left" width="20%"><c:out value="${category.name}" /></td>
                </tr>
                <c:forEach var="subcategory" items="${category.subcategories}">
                    <tr>
                        <td align="center"><c:out value="${subcategory.name}" /></td>
                    </tr>
                    <c:forEach var="goods" items="${subcategory.goods}">
                        <tr>
                            <td align="right" style="vertical-align:bottom"><c:out value="${goods.producer}" /></td>
                            <td align="center" style="vertical-align:bottom"><c:out value="${goods.model}" /></td>
                            <td align="center" style="vertical-align:bottom"><c:out value="${goods.dateOfIssue}" /></td>
                            <td align="center" style="vertical-align:bottom"><c:out value="${goods.color}" /></td>
                            <c:choose>
                                <c:when test="${not empty goods.price}">
                                    <td align="center"><c:out value="${goods.price}" /></td>
                                </c:when>
                                <c:otherwise>
                                    <td align="center"><c:out value="not-in-stock" /></td>
                                </c:otherwise>
                            </c:choose>
                        </tr>
                    </c:forEach>
                </c:forEach>
            </c:forEach>
        </table>
        <a  name="index" href="index.jsp">Home</a>
    </body>
</html>