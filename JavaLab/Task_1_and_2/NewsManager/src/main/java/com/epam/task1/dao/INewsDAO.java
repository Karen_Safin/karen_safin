package com.epam.task1.dao;

import com.epam.task1.model.News;
import java.util.List;

/**
 *
 * @author Karen_Safin
 */
public interface INewsDAO {
    public List getList();
    public int saveEditedNews(News news);
    public int saveNewNews(News news);
    public News fetchById(int newsId);
    public void remove(Integer[] id);
}
