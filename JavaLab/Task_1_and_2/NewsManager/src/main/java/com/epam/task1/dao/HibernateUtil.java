package com.epam.task1.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 *
 * @author Karen_Safin
 */
public class HibernateUtil {

    private static final Logger theLogger = Logger.getLogger(HibernateUtil.class);
    private static final SessionFactory sessionFactory;
    private static final ServiceRegistry serviceRegistry;

    private HibernateUtil() {
    }

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
            configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            System.out.println(sessionFactory.toString());
        } catch (Throwable ex) {
            theLogger.error(ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     *
     * @return
     */
    public static Session currentSession() {
        
        return sessionFactory.getCurrentSession();
    }

}