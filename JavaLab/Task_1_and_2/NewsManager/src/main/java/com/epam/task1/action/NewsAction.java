package com.epam.task1.action;

import com.epam.task1.model.News;
import com.epam.task1.dao.INewsDAO;
import com.epam.task1.form.NewsForm;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class NewsAction extends DispatchAction{

    private INewsDAO newsDAO;
    private static final String NEWS_ADD_VIEW = "addEditNews";
    private static final String PAGE_CANCEL = "pageCancel";
    private static final String NEWS_VIEW = "newsView";
    private static final String NEWS_LIST = "newsList";
    private static final String LANGUAGE = "language";
    private static final String PAGE = "page";
    
    public NewsAction() {
    }

    public INewsDAO getNewsDAO() {
        return newsDAO;
    }

    public void setNewsDAO(INewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }
    
    public ActionForward list(ActionMapping mapping, ActionForm form,
                            HttpServletRequest request, HttpServletResponse response)
                            throws Exception {
        List<News> newsList = newsDAO.getList();
        NewsForm newsForm = (NewsForm) form;
        newsForm.setNewsList(newsList);
        HttpSession ses = request.getSession();
        ses.setAttribute(PAGE, NEWS_LIST);
        return mapping.findForward(NEWS_LIST);
    }
    
    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        saveToken(request);
        NewsForm newsForm = (NewsForm) form;
        newsForm.setNewsMessage(new News(), getLocale(request));
        HttpSession ses = request.getSession();
        ses.setAttribute(PAGE, NEWS_ADD_VIEW);
        return mapping.findForward(NEWS_ADD_VIEW);
    }
    
    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        if (isTokenValid(request)) {
            resetToken(request);
            NewsForm newsForm = (NewsForm) form;
            News news = newsForm.getNewsMessage();
            int idNews;
            if(news.getId()== 0){
                idNews = newsDAO.saveNewNews(news);
            } else {
                idNews = newsDAO.saveEditedNews(news);
            }
            news.setId(idNews);
            newsForm.setNewsMessage(news, request.getLocale());
            HttpSession ses = request.getSession();
            ses.setAttribute(PAGE, NEWS_VIEW);
            return mapping.findForward(NEWS_VIEW);
        } else {
            HttpSession ses = request.getSession();
            ses.setAttribute(PAGE, NEWS_VIEW);
            return mapping.findForward(NEWS_VIEW);
        }
    }
    
    public ActionForward setLanguage(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String language = request.getParameter(LANGUAGE);
        setLocale(request, new Locale(language));
        NewsForm newsForm = (NewsForm) form;
        newsForm.setLocale(getLocale(request));
        newsForm.setNewsMessage(newsForm.getNewsMessage(), getLocale(request));
        HttpSession ses = request.getSession();
        String page = (String) ses.getAttribute(PAGE);
        if (page.length() != 0) {
            return mapping.findForward(page);
        } else {
            return mapping.findForward(NEWS_LIST);
        }
    }
    
    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        NewsForm newsForm = (NewsForm) form;
        String targetId = request.getParameter("idNews");
        int newsId = Integer.parseInt(targetId);
        News news = newsDAO.fetchById(newsId);
        newsForm.setNewsMessage(news, getLocale(request));
        HttpSession ses = request.getSession();
        ses.setAttribute(PAGE_CANCEL, NEWS_VIEW);
        ses.setAttribute(PAGE, NEWS_VIEW);
        return mapping.findForward(NEWS_VIEW);
    }
    
    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        saveToken(request);
        NewsForm newsForm = (NewsForm) form;
        String targetId = request.getParameter("idNews");
        int newsId = Integer.parseInt(targetId);
        News news = newsDAO.fetchById(newsId);
        newsForm.setNewsMessage(news, getLocale(request));
        HttpSession ses = request.getSession();
        String page = (String) ses.getAttribute(PAGE);
        if (NEWS_LIST.equals(page)) {
            ses.setAttribute(PAGE_CANCEL, NEWS_LIST);
        }
        ses.setAttribute(PAGE, NEWS_ADD_VIEW);
        return mapping.findForward(NEWS_ADD_VIEW);
    }
    
    public ActionForward cancel(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession ses = request.getSession();
        String page = (String) ses.getAttribute(PAGE);
        String pageCancle = (String) ses.getAttribute(PAGE_CANCEL);
        NewsForm newsForm = (NewsForm) form;
        int newsId = newsForm.getNewsMessage().getId();
        if ((NEWS_ADD_VIEW.equals(page)) && (newsId != 0)
                && !NEWS_LIST.equals(pageCancle)) {
            return mapping.findForward(NEWS_VIEW);
        } else {
            ses.setAttribute(PAGE_CANCEL, NEWS_LIST);
            return mapping.findForward(NEWS_LIST);
        }
    }
    
    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        NewsForm newsForm = (NewsForm) form;
        Integer[] id = new Integer[1];
        id[0] = newsForm.getNewsMessage().getId();
        newsDAO.remove(id);
        return list(mapping, form, request, response);
    }

    public ActionForward deleteGroup(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        NewsForm newsForm = (NewsForm) form;
        newsDAO.remove(newsForm.getNewsGroup());
        return list(mapping, form, request, response);
    }
}
