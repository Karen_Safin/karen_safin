package com.epam.task1.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 *
 * @author Karen_Safin
 */
public class RequestEncodeFilter implements Filter {

    private static final String PARAM_NAME_ENCODING = "encoding";
    private static final String PARAM_NAME_RESPONSE_ENCODING = "responseEncoding";
    private FilterConfig filterConfig = null;

    /**
     *
     */
    public RequestEncodeFilter() {
    }

    /**
     *
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
        FilterChain chain) throws IOException, ServletException {
        String encoding = filterConfig.getInitParameter(PARAM_NAME_ENCODING);
        String responseEncoding = filterConfig.getInitParameter(PARAM_NAME_RESPONSE_ENCODING);
        request.setCharacterEncoding(encoding);
        chain.doFilter(request, response);
        response.setContentType(responseEncoding);
    }

    /**
     *
     */
    @Override
    public void destroy() {
        this.filterConfig = null;
    }
}