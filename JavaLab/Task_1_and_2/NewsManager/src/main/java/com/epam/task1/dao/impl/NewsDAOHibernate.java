package com.epam.task1.dao.impl;

import com.epam.task1.dao.HibernateUtil;
import com.epam.task1.dao.INewsDAO;
import com.epam.task1.model.News;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Karen_Safin
 */
public class NewsDAOHibernate implements INewsDAO {

    private static final Logger theLogger = Logger.getLogger(NewsDAOHibernate.class);
    private static final String QUERY_REMOVE = "NewsDAOHibernate.remove";
    private static final String QUERY_PARAM_LIST = "list";
    private static final String QUERY_PARAM_DATE = "date";

    /**
     *
     */
    public NewsDAOHibernate() {
    }

    /**
     *
     * @return
     */
    @Override
    public List getList() {
        Session session = null;
        List<News> newsList = new ArrayList<News>();
        try {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            newsList = session.createCriteria(News.class).addOrder(Order.asc(QUERY_PARAM_DATE)).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            theLogger.error(e.getMessage());
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }

        } finally {
            if((session != null) && (session.isOpen())){
                session.close();
            }
        }
        return newsList;
    }

    /**
     *
     * @param news
     * @return
     */
    @Override
    public int saveEditedNews(News news) {
        Session session = null;
        try {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            session.update(news);
            session.getTransaction().commit();

        } catch (Exception e) {
            theLogger.error(e.getMessage());
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if((session != null) && (session.isOpen())){
                session.close();
            }
        }
        return news.getId();
    }

    /**
     *
     * @param id
     */
    @Override
    public void remove(Integer[] id) {

        Session session = null;
        try {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            Query q = session.getNamedQuery(QUERY_REMOVE);
            q.setParameterList(QUERY_PARAM_LIST, Arrays.asList(id));
            q.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            theLogger.error(e.getMessage());
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if((session != null) && (session.isOpen())){
                session.close();
            }
        }
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public News fetchById(int id) {
        Session session = null;
        News news = null;
        try {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            news = (News) session.load(News.class, id);
            session.getTransaction().commit();
        } catch (Exception e) {
            theLogger.error(e.getMessage());
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if((session != null) && (session.isOpen())){
                session.close();
            }
        }
        return news;
    }

    /**
     *
     * @param news
     * @return
     */
    @Override
    public int saveNewNews(News news) {
        Session session = null;
        try {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            session.save(news);
            session.getTransaction().commit();

        } catch (Exception e) {
            theLogger.error(e.getMessage());
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if((session != null) && (session.isOpen())){
                session.close();
            }
        }
        return news.getId();
    }
}