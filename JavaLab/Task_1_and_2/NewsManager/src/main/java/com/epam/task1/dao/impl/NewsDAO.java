package com.epam.task1.dao.impl;

import com.epam.task1.connection.impl.ConnectionPool;
import com.epam.task1.dao.INewsDAO;
import com.epam.task1.model.News;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Karen_Safin
 */
public class NewsDAO implements INewsDAO{

    private static final Logger theLogger = Logger.getLogger(NewsDAO.class);
    private static final String TABLE_NEWS_NAME = "newslist2";
    private static final String ID_NEWS = "ID";
    private static final String TITLE_NEWS = "TITLE";
    private static final String DATE_NEWS = "DATE";
    private static final String BRIEF_NEWS = "BRIEF";
    private static final String CONTENT_NEWS = "CONTENT";
    private ConnectionPool connectionPool;
    
    /**
     *
     */
    public NewsDAO() {
    }
    
    /**
     *
     * @return
     */
    public ConnectionPool getConnectionPool() {
        return connectionPool;
    }

    /**
     *
     * @param connectionPool
     */
    public void setConnectionPool(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }
    
    /**
     *
     * @return
     */
    @Override
    public List getList() {
        
        List rezultList = new ArrayList<>();
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = connectionPool.getConnection();
            ps = con.prepareStatement("SELECT * FROM " + TABLE_NEWS_NAME +
                    " ORDER BY " + DATE_NEWS + " DESC");
            rs = ps.executeQuery();
            while (rs.next()) {
                News news = new News();
                news.setId(rs.getInt(ID_NEWS));
                news.setTitle(rs.getString(TITLE_NEWS));
                news.setDate(rs.getDate(DATE_NEWS));
                news.setBrief(rs.getString(BRIEF_NEWS));
                news.setContent(rs.getString(CONTENT_NEWS));
                rezultList.add(news);
            }
        } catch (Exception e) {
            theLogger.error(e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    theLogger.error(e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    theLogger.error(e);
                }
            }
            if (con != null) {
                try {
                    connectionPool.releaseConnection(con);
                } catch (SQLException ex) {
                    theLogger.error(ex);
                }
            }
        }
        return rezultList;
    }

    /**
     *
     * @param news
     * @return
     */
    @Override
    public int saveEditedNews(News news) {
        PreparedStatement ps = null;
        Connection con = null;
        CallableStatement call = null;
        int idNews;
        try {
            con = connectionPool.getConnection();
                ps = con.prepareStatement(
                        "UPDATE " + TABLE_NEWS_NAME + " SET "
                                + TITLE_NEWS + "= ?, "
                                + DATE_NEWS + "= ?, "
                                + BRIEF_NEWS + "= ?, "
                                + CONTENT_NEWS + "= ? WHERE "
                                + ID_NEWS + "= ?");
                ps.setString(1, news.getTitle());
                ps.setDate(2, (Date) news.getDate());
                ps.setString(3, news.getBrief());
                ps.setString(4, news.getContent());
                ps.setInt(5, news.getId());
                ps.executeUpdate();
                idNews = news.getId();
            
        } catch (Exception e) {
            theLogger.error(e.getMessage());
            return 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    theLogger.error(e);
                }
            }
            if (call != null) {
                try {
                    call.close();
                } catch (SQLException ex) {
                    theLogger.error(ex);
                }
            }
            if (con != null) {
                try {
                    connectionPool.releaseConnection(con);
                } catch (SQLException ex) {
                    theLogger.error(ex);
                }
            }
        }
        return idNews;
    }
    
    /**
     *
     * @param id
     * @return
     */
    @Override
    public News fetchById(int id) {
        News rez = new News();
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = connectionPool.getConnection();
            ps = con.prepareStatement("SELECT * FROM " + TABLE_NEWS_NAME + " WHERE " + ID_NEWS + "=?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            rs.next();
            rez.setId(rs.getInt(ID_NEWS));
            rez.setTitle(rs.getString(TITLE_NEWS));
            rez.setDate(rs.getDate(DATE_NEWS));
            rez.setBrief(rs.getString(BRIEF_NEWS));
            rez.setContent(rs.getString(CONTENT_NEWS));
        } catch (Exception e) {
            theLogger.error(e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    theLogger.error(e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    theLogger.error(e);
                }
            }
            if (con != null) {
                try {
                    connectionPool.releaseConnection(con);
                } catch (SQLException ex) {
                    theLogger.error(ex);
                }
            }
        }
        return rez;
    }

    /**
     *
     * @param id
     */
    @Override
    public void remove(Integer[] id) {
        Statement st = null;
        Connection con = null;
        try {
            con = connectionPool.getConnection();
            if (id != null) {
                st = con.createStatement();
                StringBuilder query = new StringBuilder();
                query.append("DELETE FROM " + TABLE_NEWS_NAME + " WHERE " + ID_NEWS + " IN (");
                query.append(id[0]);
                for (int i = 1; i < id.length; i++) {
                    query.append(", ");
                    query.append(id[i]);
                }
                query.append(")");
                st.executeUpdate(query.toString());
            }
        } catch (Exception e) {
            theLogger.error(e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    theLogger.error(ex.getMessage());
                }
            }
            if (con != null) {
                try {
                    connectionPool.releaseConnection(con);
                } catch (SQLException ex) {
                    theLogger.error(ex.getMessage());
                }
            }
        }
    }

    /**
     *
     * @param news
     * @return
     */
    @Override
    public int saveNewNews(News news) {
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet rs;
        CallableStatement call = null;
        int idNews;
        try {
            con = connectionPool.getConnection();
                ps = con.prepareStatement(
                        "INSERT INTO " + TABLE_NEWS_NAME+" ("+ DATE_NEWS+","+
                        TITLE_NEWS+","+ BRIEF_NEWS+","+ CONTENT_NEWS+")" +
                        " VALUES (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
                ps.setDate(1, (Date) news.getDate());
                ps.setString(2, news.getTitle());
                ps.setString(3, news.getBrief());
                ps.setString(4, news.getContent());
                ps.executeUpdate();
                rs = ps.getGeneratedKeys();
                rs.next();
                idNews = rs.getInt("GENERATED_KEY");
            
        } catch (Exception e) {
            theLogger.error(e.getMessage());
            return 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    theLogger.error(e);
                }
            }
            if (call != null) {
                try {
                    call.close();
                } catch (SQLException ex) {
                    theLogger.error(ex);
                }
            }
            if (con != null) {
                try {
                    connectionPool.releaseConnection(con);
                } catch (SQLException ex) {
                    theLogger.error(ex);
                }
            }
        }
        return idNews;
    }
}
