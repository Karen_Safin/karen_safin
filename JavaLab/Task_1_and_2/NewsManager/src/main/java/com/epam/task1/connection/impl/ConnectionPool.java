package com.epam.task1.connection.impl;

import com.epam.task1.connection.IConnectionPool;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.apache.log4j.Logger;

/**
 *
 * @author Karen_Safin
 */
public final class ConnectionPool implements IConnectionPool{
    
    private static final Logger theLogger = Logger.getLogger(ConnectionPool.class);
    private static final String USER_PARAMETER = "user";
    private static final String PASSWORD_PARAMETER = "password";
    private final String driverClassName;
    private final String username;
    private final String password;
    private final String url;
    private final Integer maxConnectionsCount;
    private final List<Connection> allConnectionList = new ArrayList<>();
    private BlockingQueue<Connection> connectionQueue = null;

    /**
     *
     * @param driverClassName
     * @param url
     * @param username
     * @param password
     * @param maxConnectionsCount
     */
    public ConnectionPool(String driverClassName, String url, String username, 
                        String password, Integer maxConnectionsCount) {
        this.driverClassName = driverClassName;
        this.url = url;
        this.username = username;
        this.password = password;
        this.maxConnectionsCount = maxConnectionsCount;
        init();
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    @Override
    public Connection getConnection() throws SQLException {
        Connection connection = null;
        try {
            connection = connectionQueue.take();
        } catch (InterruptedException ex) {
            theLogger.error(ex.getMessage());
        }
        return connection;
    }

    /**
     *
     * @param c
     * @throws SQLException
     */
    @Override
    public void releaseConnection(Connection c) throws SQLException {
            connectionQueue.add(c);
    }

    private Properties initProperty() throws SQLException {
        Properties properties = new Properties();
        try {
            Class.forName(driverClassName);
            properties.setProperty(USER_PARAMETER, username);
            properties.setProperty(PASSWORD_PARAMETER, password);
            properties.setProperty("useUnicode", "true");
        } catch (ClassNotFoundException ex) {
            theLogger.error(ex.getMessage());
        }
        return properties;
    }

    /**
     *
     */
    @Override
    public void closeAll() {
        for (Connection c : allConnectionList) {
            try {
                c.close();
            } catch (SQLException ex) {
                theLogger.error(ex.getMessage());
            }
        }
    }

    @Override
    public void init() {
        try {
            connectionQueue = new ArrayBlockingQueue<>(maxConnectionsCount);
            Properties properties = initProperty();
            Connection connection = DriverManager.getConnection(url, properties);
            for (int i = 0; i < maxConnectionsCount; i++) {                    
                this.connectionQueue.add(connection);
                this.allConnectionList.add(connection);
            }
        } catch (SQLException ex) {
            theLogger.error(ex.getMessage());
        }
    }
}
