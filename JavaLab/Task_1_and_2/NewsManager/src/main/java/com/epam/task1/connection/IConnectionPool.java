/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.task1.connection;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Karen_Safin
 */
public interface IConnectionPool {
    public Connection getConnection() throws SQLException;
    public void releaseConnection(Connection c) throws SQLException;
    public void closeAll();
    public void init();
}
