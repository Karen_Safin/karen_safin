package com.epam.task1.form;

import com.epam.task1.model.News;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Karen_Safin
 */
public class NewsForm extends org.apache.struts.action.ActionForm {

    private News newsMessage = new News();
    private List newsList;
    private String dateString = new String();
    private Integer[] newsGroup;
    private Locale locale = Locale.getDefault();
    private static final String BASE_NAME = "properties.ApplicationResource";
    private static final String DATE_PATTERN = "date.pattern";

    /**
     *
     * @param dateString
     */
    public void setDateString(String dateString) {
        ResourceBundle rb = ResourceBundle.getBundle(BASE_NAME,locale);
        String datePattern = rb.getString(DATE_PATTERN);
        SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
        try {
            Date date = dateFormat.parse(dateString);
            java.sql.Date dateSql = new java.sql.Date(date.getTime());
            newsMessage.setDate(dateSql);
        } catch (ParseException ex) {
            Logger.getLogger(NewsForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dateString = dateString;
    }

    /**
     *
     * @return
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     *
     * @param locale
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     *
     * @return
     */
    public String getDateString() {
        return dateString;
    }

    /**
     *
     * @param newsList
     */
    public void setNewsList(List newsList) {
        this.newsList = newsList;
    }

    /**
     *
     * @param newsMessage
     * @param locale
     */
    public void setNewsMessage(News newsMessage, Locale locale) {
        ResourceBundle rb = ResourceBundle.getBundle(BASE_NAME, locale);
        String datePattern = rb.getString(DATE_PATTERN);
        SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
        this.dateString = dateFormat.format(newsMessage.getDate());
        this.newsMessage = newsMessage;
    }

    /**
     *
     * @param newsGroup
     */
    public void setNewsGroup(Integer[] newsGroup) {
        this.newsGroup = newsGroup;
    }

    /**
     *
     * @return
     */
    public List getNewsList() {
        return newsList;
    }

    /**
     *
     * @return
     */
    public News getNewsMessage() {
        return newsMessage;
    }

    /**
     *
     * @return
     */
    public Integer[] getNewsGroup() {
        return newsGroup;
    }

}