function confirmDelete(path) {
    if(confirm(confMessage)) {
        if(path != null)
            location.replace(path);
        return true;
    } else {
        return false;
    }
}

function validateGroupDelete(form) {
    var checkbox;
    for(var i=0; i<form.elements.length; i++) {
        checkbox = form.elements[i];
        if(checkbox.checked) {
            return confirmDelete(null);
        }
    }
    alert(groupDel);
    return false;
}
var dtCh= "/";
var minYear=1900;
var maxYear=2015;

function isInteger(s){
    var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31
        if (i==4 || i==6 || i==9 || i==11) {
            this[i] = 30
        }
        if (i==2) {
            this[i] = 29
        }
    }
    return this
}
   
function isDate(dtStr){
    var daysInMonth = DaysArray(12);
    var pos1=dtStr.indexOf(dtCh);
    var pos2=dtStr.indexOf(dtCh,pos1+1);
    var strMonth, strDay, strYear;
    if("MM/dd/yyyy" == formatDate) {
        strMonth=dtStr.substring(0,pos1);
        strDay=dtStr.substring(pos1+1,pos2);
        strYear=dtStr.substring(pos2+1);
    } else {
        strDay=dtStr.substring(0,pos1);
        strMonth=dtStr.substring(pos1+1,pos2);
        strYear=dtStr.substring(pos2+1);
    }
    strYr=strYear
    if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
    if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
    }
    month=parseInt(strMonth)
    day=parseInt(strDay)
    year=parseInt(strYr)
    if (pos1==-1 || pos2==-1){
        alert(errorText[5])
        return false
    }
    if (strMonth.length<1 || month<1 || month>12){
        alert(errorText[8])
        return false
    }
    if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
        alert(errorText[9])
        return false
    }
    if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
        alert(errorText[10]+minYear+ errorText[11] + maxYear)
        return false
    }
    if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
        alert(errorText[12])
        return false
    }
    return true
}




function validateAddEditNews(form)
{
    var el,
    elName,
    value,
    type;
    for (var i = 0; i < form.elements.length; i++) {
        el = form.elements[i];
        elName = el.nodeName.toLowerCase();
        value = el.value;
        if (elName == "input") { // INPUT
            type = el.type.toLowerCase();
            switch (type) {
                case "text" :
                    if (el.name == "newsMessage.title") {
                        if(value == "") {
                            alert(errorText[1]);
                            el.focus();
                            return false;
                        } 
                        if(value.length > 100) {
                            alert(errorText[14]);
                            el.focus();
                            return false;
                        }
                    }

                    if (el.name == "dateString")
                    {                        
                        if (isDate(value,formatDate)==false){
                            el.focus();
                            return false
                        }
                    }
                    break;
                default :
                    // type = hidden, submit, button, image
                    break;
            }
        } else if (elName == "textarea") {
            if (el.name == "newsMessage.brief") {
                if(value == ""){
                    alert(errorText[3]);
                    el.focus();
                    return false;
                }
                if(value.length > 500) {
                    alert(errorText[6]);
                    el.focus();
                    return false;
                }
            }
            if (el.name == "newsMessage.content") {
                if(value == ""){
                    alert(errorText[4]);
                    el.focus();
                    return false;
                }

                if(value.length > 2048){
                    alert(errorText[7]);
                    el.focus();
                    return false;
                }
            }
        }
    }
    return true;

}