<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" href="css/style.css" rel="stylesheet" />
        <script src="js/news.js" type="text/javascript"></script>
        <script type="text/javascript">
            var errorText = { <bean:message key="vars.add-edit"/> } ;
            var formatDate = "<bean:message key="date.pattern"/>";
            var groupDel = "<bean:message key="vars.groupDel"/>";
            var confMessage = "<bean:message key="vars.confMessage"/>";
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <tiles:insert attribute="header"/>
            </div>
            <div id="content">
                <div id="leftcol">
                    <tiles:insert attribute="menu"/>
                </div>
                <div id="rightcol">                    
                    <div id="contentBody">
                        <tiles:insert attribute="body"/>
                    </div>
                </div>
                <div class="push"></div>
            </div>
        </div>
        <div id='footer'>
            <tiles:insert attribute="footer"/>
        </div>
    </body>
</html>