<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>

<title><nested:message key="head-body.title.list"/></title>
<div id="currentPage">
    <nested:message key="content.workflow" arg0="News List"/>
</div>
<ul>

    <html:form action="Action.do?method=deleteGroup"  onsubmit="return validateGroupDelete(this)" >
        
        <nested:iterate name="newsForm" property="newsList">
            <li>
                <p class="datelist"><nested:write property="date" formatKey="date.pattern"/></p>

                <h3 class="titlelist">
                    <nested:write property="title"/>
                </h3>
                <div class="clear"></div>
                <p class="brieflist"><nested:write property="brief"/></p>
                <p class="clicklist">
                    
                    <nested:define id="newsId"  property="id"/>

                    <html:link paramName="newsId" paramId="idNews" action="/Action.do?method=view">
                        <nested:message key="content.news.view"/>
                    </html:link>
                    <html:link paramName="newsId" paramId="idNews" action="/Action.do?method=edit">
                        <nested:message key="content.news.edit"/>
                    </html:link>
                    <html:multibox name="newsForm" property="newsGroup" >
                        <nested:write property="id"/>
                    </html:multibox>
                </p>
                <div class="clear"></div>
            </li>
        </nested:iterate>
    
    <div class="clear"></div>
    <bean:size id="ListSize"
               name="newsForm"
               property="newsList"/>
    <logic:notMatch  name="ListSize" value="0">
        <div id="delete">
            <html:submit>
                <nested:message key="content.button.delete"/>
            </html:submit>
        </div>
    </logic:notMatch>
</html:form>
</ul>