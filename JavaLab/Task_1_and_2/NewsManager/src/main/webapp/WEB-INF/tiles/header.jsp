<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<h2>
    <bean:message key="header.text"/>
</h2>
<p id="language">
    <html:link page="/Action.do?method=setLanguage&language=en">
        <bean:message key="header.language.en"/>
    </html:link>
    <html:link page="/Action.do?method=setLanguage&language=ru">
        <bean:message key="header.language.ru"/>
    </html:link>
</p>