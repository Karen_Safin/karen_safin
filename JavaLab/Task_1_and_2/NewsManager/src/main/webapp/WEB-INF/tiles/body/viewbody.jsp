<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>

<title><nested:message key="head-body.title.view"/></title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>

<div id="currentPage">
    <nested:message key="content.workflow" arg0="News View"/>
</div>
<table cellspacing="30px">
    <tr>
        <td width="100px" valign="top">
            <p id="titleText"><nested:message key="content.news.title"/></p>
        </td>
        <td>
            <p class="titlelist"><nested:write name="newsForm" property="newsMessage.title"/></p>
        </td>
    </tr>
    <tr>
        <td  valign="top">
            <p id="titleText"><nested:message key="content.news.date"/></p>
        </td>
        <td>
            <p class="datelist"><nested:write name="newsForm" property="newsMessage.date" formatKey="date.pattern"/></p>
        </td>
    </tr>
    <tr>
        <td  valign="top">
            <p id="titleText"><nested:message key="content.news.brief"/></p>
        </td>
        <td>
            <p class="breaf"><nested:write name="newsForm" property="newsMessage.brief"/></p>
        </td>
    </tr>
    <tr>
        <td  valign="top">
            <p id="titleText"><nested:message key="content.news.content"/></p>
        </td>
        <td>
            <p class="content"><nested:write name="newsForm" property="newsMessage.content"/></p>
        </td>
    </tr>
</table>

<div id="delete_edit">
    <input type="button" 
           value="<nested:message key="content.button.edit"/>" 
           onclick="location.replace('Action.do?method=edit&idNews='+<nested:write name="newsForm" property="newsMessage.id"/>);"/>
    <input type="button" 
           id="butDelete" 
           onclick="return confirmDelete('Action.do?method=delete&idNews='+<nested:write name="newsForm" property="newsMessage.id"/>)" 
           value="<nested:message key="content.button.delete"/>"/>
</div>

<div class="clear"></div>
