<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<h2>
    <bean:message key="content.menu.title"/>
</h2>
<ul>
	<li>
            <a href="Action.do?method=list">
                <bean:message key="content.menu.newslist"/>
            </a>
        </li>
	<li>
            <a href="Action.do?method=add">
                <bean:message key="content.menu.addnews"/>
            </a>
        </li>
</ul>