<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="org.apache.struts.taglib.html.Constants"%>

<title><bean:message key="head-body.title.add-edit"/></title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>

<div id="workflow">
    <bean:message key="content.workflow" arg0="Add News"/>
</div>

<html:form action="Action.do?method=save" onsubmit="return validateAddEditNews(this)">

    <html:hidden name="newsForm" property="newsMessage.id"/>
    <table id="table_edit" >
        <tr class="trAdd">
            <td width="100px" valign="top" class="tdAdd">
                <bean:message key="content.news.title"/>
            </td>
            <td class="tdAdd">
                <html:text name="newsForm" property="newsMessage.title" maxlength="150" size="70"/>
            </td>
        </tr>
        <tr class="trAdd">
            <td  valign="top" class="tdAdd">
                <p><bean:message key="content.news.date" /></p>
            </td>
            <td class="tdAdd">
                <html:text name="newsForm" property="dateString"   maxlength="10" size="10" />
            </td>
        </tr>
        <tr class="trAdd">
            <td  valign="top" class="tdAdd">
                <p><bean:message key="content.news.brief"/></p>
            </td>
            <td class="tdAdd">
                <html:textarea name="newsForm" property="newsMessage.brief" rows="8" cols="72"/>
            </td>
        </tr>
        <tr class="trAdd">
            <td valign="top" class="tdAdd">
                <p><bean:message key="content.news.content"/></p>
            </td>
            <td class="tdAdd">
                <html:textarea name="newsForm" property="newsMessage.content" rows="8" cols="72"/>
            </td>
        </tr>
    </table>
    <p id="save_cancel">
        <html:submit property="save" >
            <bean:message key="content.button.save"/>
        </html:submit>
        <input type="button" id="butCancle" onclick="location.replace('Action.do?method=cancel')" value="<bean:message key="content.button.cancel"/>"/>
    </p>
    <div class="clear"></div>
</html:form>